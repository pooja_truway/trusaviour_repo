//
//  ApiManager.swift
//  saviour
//
//  Created by Pooja on 30/06/17.
//  Copyright © 2017 Truway. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol apiManagerDelegate:NSObjectProtocol {
    
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>)
    func APIFailureResponse(_ msgError: String)
}

extension apiManagerDelegate {
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
        print ("hello apimanager \(response)")
    }
    
    func APIFailureResponse(_ msgError: String){
        // leaving this empty
    }
}

class ApiManager: NSObject,apiManagerDelegate {
    
    fileprivate let API_STATUS = "error_status"
    var delegate :apiManagerDelegate?
    var appConstants: AppConstants = AppConstants()
    
    // MARK: HUD Utility
    func showHud()  {
        // HUD.flash(.rotatingImage(UIImage(named: "progress")), delay: 2.0)
    }
    func showlongHud()  {
        // HUD.flash(.rotatingImage(UIImage(named: "progress")), delay: 5.0)
    }
    
    func hideHud()  {
        
    }
    //MARK: Send Contact Details To Server
    func ApiSendInfoToServer (action:String,userName:String,userMobile:String,userBloodGroup:String,contact1Name:String,contact1Mobile:String,contact1Relation:String,contact2Name:String,contact2Mobile:String,contact2Relation:String,contact3Name:String,contact3Mobile:String,contact3Relation:String,device_token:String){
        
        Alamofire.request(appConstants.BaseURL, method: .post, parameters: ["action":action,"userName":userName,"userMobile":userMobile,"userBloodGroup":userBloodGroup,"contact1Name":contact1Name,"contact1Mobile":contact1Mobile,"contact1Relation":contact1Relation,"contact2Name":contact2Name,"contact2Mobile":contact2Mobile,"contact2Relation":contact2Relation,"contact3Name":contact3Name,"contact3Mobile":contact3Mobile,"contact3Relation":contact3Relation,"device_token":device_token], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
}
    
    //MARK: Get list of trauma centers
    func ApiGetTraumaCenters (latitude: Double,longitude:Double){
        
      let url =  "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(latitude),\(longitude)&radius=1000&type=hospital&key=AIzaSyCDTUYCSXEiUN8cM7oiU4DM5CRLCnKxkdo"
        print(url)
        Alamofire.request(url, method: .get, parameters:nil, encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
    
    //MARK: Draw api routes
    func ApiDrawRoutes (origin: String,destination:String){
        
        
    let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=AIzaSyC4uu4EerkD3ItFh5gWLdP0wG2SgFa-hE4"

        print(url)
        Alamofire.request(url, method: .get, parameters:nil, encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
    
    //MARK: Save Profile Info
    
    func ApiSaveProfileInfoToServer (action:String,userName:String,userMobile:String,userBloodGroup:String,device_token:String,deviceId: String,userId:String){
        
        Alamofire.request(appConstants.BaseURL, method: .post, parameters: ["action":action,"userName":userName,"userMobile":userMobile,"userBloodGroup":userBloodGroup,"device_token":device_token,"deviceId":deviceId,"userId":userId], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
    
    //MARK: Api to Send Additional Details To Server
    func ApiSendAdditionalDetailsToServer (action:String,userName:String,userMobile:String,userBloodGroup:String,userId:Int,location: String,city:String,state:String,country: String,isReadyToDonateBlood:String,duration:String,alergy:String,diseases:String,donateOrgan:String,organList:String){
        
        Alamofire.request(appConstants.BaseURL, method: .post, parameters: ["action":action,"userName":userName,"userMobile":userMobile,"userBloodGroup":userBloodGroup,"userId":userId,"location":location,"city":city,"state":state,"country":country,"isReadyToDonateBlood":isReadyToDonateBlood,"duration":duration,"alergy":alergy,"diseases":diseases,"donateOrgan":donateOrgan,"organList":organList], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
    
    //MARK: Get MY Donor Lost From Server
    func ApiGetMyDonorList (action:String,userId:Int){
        
        Alamofire.request(appConstants.BaseURL, method: .post, parameters: ["action":action,"userId":userId], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
    
    //MARK: Api Update My Own Donor List
    func ApiUpdateMyDonorList (action:String,userId:Int,donorId:String,donorName:String,donorMobile:String,donorBloodGroup:String,donorCity:String,donorState:String,donorCountry:String){
        
        Alamofire.request(appConstants.BaseURL, method: .post, parameters: ["action":action,"userId":userId,"donorId":donorId,"donorName":donorName,"donorMobile":donorMobile,"donorBloodGroup":donorBloodGroup,"donorCity":donorCity,"donorState":donorState,"donorCountry":donorCountry], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
    
    //MARK: Api Delete My Own Donor List
    func ApiDeleteMyDonorList (action:String,userId:Int,donorId:String){
        
        Alamofire.request(appConstants.BaseURL, method: .post, parameters: ["action":action,"userId":userId,"donorId":donorId], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
    
    //MARK: Api Find Donor List
    func ApiFindDonorList (action:String,userId:Int,location: String,bloodGroup:String,city:String,state:String,country:String){
        
        Alamofire.request(appConstants.BaseURL, method: .post, parameters: ["action":action,"userId":userId,"location":location,"bloodGroup":bloodGroup,"city":city,"state":state,"country":country], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }

    //MARK: Api Find Organ Donor List
    func ApiFindOrganDonorList (action:String,userId:Int,location: String,bloodGroup:String,city:String,state:String,country:String,organ: String){
        
        Alamofire.request(appConstants.BaseURL, method: .post, parameters: ["action":action,"userId":userId,"location":location,"bloodGroup":bloodGroup,"city":city,"state":state,"country":country,"organ":organ], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
    
    //MARK: Api to report message send to contacts
    func SendMessageReportApiCall (action:String,userId:Int,message:String,toNumber:String,latitude:Double,longitude:Double,time:String){
        
       // let params : String = ("\("action":action,"userId":userId,"message":message,"toNumber":toNumber,"latitude":latitude,"longitude":longitude,"time":time)")
        
        Alamofire.request(appConstants.BaseURL, method: .post, parameters: ["action":action,"userId":userId,"message":message,"toNumber":toNumber,"latitude":latitude,"longtitude":longitude,"time":time], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }



}
