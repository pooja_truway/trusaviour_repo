//
//  AppConstants.swift
//  saviour
//
//  Created by Pooja on 27/06/17.
//  Copyright © 2017 Truway. All rights reserved.
//

import Foundation
import UIKit

class AppConstants {
    
    // MARK: - Application Constants
    var screenWidth = UIScreen.main.bounds.width
    var screenHeight = UIScreen.main.bounds.height
    let defaults = UserDefaults.standard
    
    //MARK: - Color Codes
    let CornerRadius: CGFloat = 5.0
    let nameInfoView_color = "#71b3a7"
    let nameInfoView_brighterColor = "#497972"
    let Red_color = "#e01f25"
    
    //MARK: Base URL
    let BaseURL = "http://www.paymny.today/webservices"
    
    //MARK: - To show Alert View
    func showAlert(title: String , message: String, controller: UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        controller.present(alert, animated: true, completion: nil)
    }
    
    //MARK: To show progress HUD
    func showLoadingHUD(to_view: UIView) {
        let hud = MBProgressHUD.showAdded(to: to_view, animated: true)
        // hud.label.text = "Loading..."
    }
    
    //MARK: To hide progress HUD
    func hideLoadingHUD(for_view: UIView) {
        MBProgressHUD.hideAllHUDs(for: for_view, animated: true)
    }
}
