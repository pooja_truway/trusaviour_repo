//
//  AppDelegate.swift
//  saviour
//
//  Created by Pooja on 27/06/17.
//  Copyright © 2017 Truway. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import KeychainAccess
import Firebase
import FirebaseMessaging
import UserNotifications


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
  // let googleMapsApiKey = "AIzaSyC4uu4EerkD3ItFh5gWLdP0wG2SgFa-hE4"
    let googleMapsApiKey = "AIzaSyCDTUYCSXEiUN8cM7oiU4DM5CRLCnKxkdo"
    let gcmMessageIDKey = "gcm.message_id"
    var appConstants : AppConstants = AppConstants()
    var bundleID = ""
    var uuidValue = ""
    var notificationArray : NSMutableArray = []

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        
        if #available(iOS 10.0, *) {
            let authOptions : UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_,_ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            // For iOS 10 data message (sent via FCM)
            Messaging.messaging().delegate = self
        }
        
        bundleID = Bundle.main.bundleIdentifier!
        let keychain = Keychain(service: bundleID)
        do {
            let token = try keychain.get(bundleID)
            print(token ?? 2)
            if ((token == nil)||(token == ""))
            {
                print("token is 0 \(String(describing: token))")
                setVenderId()
            }
            else{
                appConstants.defaults.set(token, forKey: "deviceID")
            }
        } catch let error {
            print("error: \(error)")
        }

        
        application.registerForRemoteNotifications()

        // Override point for customization after application launch.
         IQKeyboardManager.sharedManager().enable = true
        GMSServices.provideAPIKey(googleMapsApiKey)
        return true
    }
    
    //MARK: - setVenderId and getVenderId
    func setVenderId() {
        let keychain = Keychain(service: bundleID)
        do {
            bundleID = Bundle.main.bundleIdentifier!
            uuidValue = UIDevice.current.identifierForVendor!.uuidString
            try keychain.set(uuidValue, key: bundleID)
            appConstants.defaults.set(uuidValue, forKey: "deviceID")
            print("venderId set : key \(bundleID) and value: \(uuidValue)")
        }
        catch let error {
            print("Could not save data in Keychain : \(error)")
        }
    }
    
    func getVenderId() -> String {
        let keychain = Keychain(service: bundleID)
        let token : String = try! keychain.get(bundleID)!
        print("token : \(token)")
        return token
    }

    //MARK: Notification delegate methods
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print("1 : \(userInfo)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print("2 : \(userInfo)")
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    // [END receive_message]
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        let token = String(format: "%@", deviceToken as CVarArg).trimmingCharacters(in: CharacterSet(charactersIn: "<>")).replacingOccurrences(of: " ", with: "")
        print("device token for notification \(token)")
        appConstants.defaults.set(token, forKey: "notification_token")
        // With swizzling disabled you must set the APNs token here.
        // Messaging.messaging().apnsToken = deviceToken
    }
}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler(UNNotificationPresentationOptions.alert)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
            for (key, value) in userInfo {
                print("key=\(key), value=\(value)")
            }
        }
        // Print full message.
        print("4 : \(userInfo)")
        
        let notificationDict : NSDictionary = userInfo as NSDictionary
      
        notificationArray.insert(notificationDict, at: 0)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let NotificationVC = storyboard.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        NotificationVC.notificationArr = notificationArray
        
        print("arry: \(notificationArray)")
        appConstants.defaults.set(notificationArray, forKey: "notifyArray")
   
        completionHandler()
    }
}
// [END ios_10_message_handling]

extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
    }
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}

@available(iOS 10.0, *)
func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
{
    
    print("app is in foreground")
    completionHandler([.alert, .badge, .sound])
}

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

