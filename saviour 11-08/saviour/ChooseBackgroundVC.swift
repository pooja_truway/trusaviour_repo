//
//  ChooseBackgroundVC.swift
//  saviour
//
//  Created by Pooja on 29/06/17.
//  Copyright © 2017 Truway. All rights reserved.
//

import UIKit

class ChooseBackgroundVC: UIViewController, UICollectionViewDelegate,UICollectionViewDataSource,UIImagePickerControllerDelegate ,UINavigationControllerDelegate{
    
    @IBOutlet weak var collectionView : UICollectionView?
    
    var appConstants : AppConstants = AppConstants()
    var imagePicker = UIImagePickerController()
    
    let WallpaperImageArray = [UIImage(named: "screen14"),
        UIImage(named: "screen1"),UIImage(named: "screen2"),
        UIImage(named: "screen3"),UIImage(named: "screen4"),UIImage(named: "screen5"),UIImage(named: "screen6"),UIImage(named: "screen7"),UIImage(named: "screen8"),UIImage(named:"screen9")
        ,UIImage(named: "screen10")
        ,UIImage(named: "screen11")
        ,UIImage(named: "screen12")
        ,UIImage(named: "screen13")]
    
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // MARK: - UICollectionViewDataSource protocol
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return WallpaperImageArray.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let reuseIdentifier = "cell"
        var cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        
            // get a reference to our storyboard cell
            let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! BackgroundImagesCell
        
            cell = cellA
            cellA.WallpaperImage.image = WallpaperImageArray[indexPath.item]
               return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
    let template1  = appConstants.defaults.value(forKey: "template1")as! Bool
       // let template2 : Bool = (appConstants.defaults.value(forKey: "template2") as! Bool
        
        print("You selected cell #\(indexPath.item)!")
        print("selected image #\(String(describing: WallpaperImageArray[indexPath.item]))")
        let contactScreen : Int = appConstants.defaults.value(forKey: "numberofContactsAdded") as! Int
        
        print("contact screen to show : \(contactScreen)")
        
        if(template1)
            {
                if(indexPath.item == 0)
                {
                    print("open gallery")
                    if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                        print("Button capture")
                        imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
                        imagePicker.sourceType = .savedPhotosAlbum;
                        imagePicker.allowsEditing = false
                        self.present(imagePicker, animated: true, completion:nil)
                    }
                    
                }
    else
                {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if(contactScreen == 3)
        {
        let LockScreen1VC = storyboard.instantiateViewController(withIdentifier: "LockScreen1VC") as! LockScreen1VC
        LockScreen1VC.selected_Image = WallpaperImageArray[indexPath.item]
        self.navigationController?.pushViewController(LockScreen1VC, animated: true)
        }
        else if(contactScreen == 2)
        {
            let Show2MobileVC = storyboard.instantiateViewController(withIdentifier: "Show2MobileVC") as! Show2MobileVC
            Show2MobileVC.selected_Image = WallpaperImageArray[indexPath.item]
            self.navigationController?.pushViewController(Show2MobileVC, animated: true)
            
            }
        else if(contactScreen == 1)
        {
            let Show1MobileVC = storyboard.instantiateViewController(withIdentifier: "Show1MobileVC") as! Show1MobileVC
            Show1MobileVC.selected_Image = WallpaperImageArray[indexPath.item]
            self.navigationController?.pushViewController(Show1MobileVC, animated: true)
        }
    }
}
        else
        {
            if(indexPath.item == 0)
            {
                print("open gallery")
                if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                    print("Button capture")
                    
                    imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
                    imagePicker.sourceType = .savedPhotosAlbum;
                    imagePicker.allowsEditing = false
                    
                    self.present(imagePicker, animated: true, completion: nil)
            }
        }
    else{
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if(contactScreen == 3)
            {
            let LockScreen2VC = storyboard.instantiateViewController(withIdentifier: "LockScreen2VC") as! LockScreen2VC
            LockScreen2VC.selected_Image = WallpaperImageArray[indexPath.item]
            self.navigationController?.pushViewController(LockScreen2VC, animated: true)
            }
        else if(contactScreen == 2)
        {
            let Show2ContactsVC = storyboard.instantiateViewController(withIdentifier: "Show2ContactsVC") as! Show2ContactsVC
            Show2ContactsVC.selected_Image = WallpaperImageArray[indexPath.item]
            self.navigationController?.pushViewController(Show2ContactsVC, animated: true)
                    
                }
        else if(contactScreen == 1)
        {
            let Show1ContactVC = storyboard.instantiateViewController(withIdentifier: "Show1ContactVC") as! Show1ContactVC
            Show1ContactVC.selected_Image = WallpaperImageArray[indexPath.item]
            self.navigationController?.pushViewController(Show1ContactVC, animated: true)
                }
        }
    }
}
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            let template1  = appConstants.defaults.value(forKey: "template1")as! Bool
            let contactScreen : Int = appConstants.defaults.value(forKey: "numberofContactsAdded") as! Int

            if(template1)
            {
               let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if(contactScreen == 3)
            {
                let LockScreen1VC = storyboard.instantiateViewController(withIdentifier: "LockScreen1VC") as! LockScreen1VC
                LockScreen1VC.selected_Image = chosenImage
                self.navigationController?.pushViewController(LockScreen1VC, animated: true)
                    }
           else if(contactScreen == 2)
            {
                let Show2MobileVC = storyboard.instantiateViewController(withIdentifier: "Show2MobileVC") as! Show2MobileVC
                Show2MobileVC.selected_Image = chosenImage
                self.navigationController?.pushViewController(Show2MobileVC, animated: true)
                        
                    }
            else if(contactScreen == 1)
            {
            let Show1MobileVC = storyboard.instantiateViewController(withIdentifier: "Show1MobileVC") as! Show1MobileVC
            Show1MobileVC.selected_Image = chosenImage
                self.navigationController?.pushViewController(Show1MobileVC, animated: true)
                    }
                }
            
            else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                if(contactScreen == 3)
                {
                    let LockScreen2VC = storyboard.instantiateViewController(withIdentifier: "LockScreen2VC") as! LockScreen2VC
                    LockScreen2VC.selected_Image = chosenImage
                    self.navigationController?.pushViewController(LockScreen2VC, animated: true)
                }
                else if(contactScreen == 2)
                {
                    let Show2ContactsVC = storyboard.instantiateViewController(withIdentifier: "Show2ContactsVC") as! Show2ContactsVC
                    Show2ContactsVC.selected_Image = chosenImage
                    self.navigationController?.pushViewController(Show2ContactsVC, animated: true)
                    
                }
                else if(contactScreen == 1)
                {
                    let Show1ContactVC = storyboard.instantiateViewController(withIdentifier: "Show1ContactVC") as! Show1ContactVC
                    Show1ContactVC.selected_Image = chosenImage
                    self.navigationController?.pushViewController(Show1ContactVC, animated: true)
                }
            }        }
        else{
            print("Something went wrong")
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    //MARK: Back Button
    @IBAction func BackClicked(sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
