//
//  ChooseTempleteVC.swift
//  saviour
//
//  Created by Pooja on 28/06/17.
//  Copyright © 2017 Truway. All rights reserved.
//

import UIKit

class ChooseTempleteVC: UIViewController {
    @IBOutlet weak var Template1_Image : UIImageView?
    @IBOutlet weak var Template2_Image : UIImageView?
    
    @IBOutlet weak var Name_View1 : UIView?
    @IBOutlet weak var Name_View2 : UIView?
    @IBOutlet weak var title_view1 : UIView?
    @IBOutlet weak var title_View2 : UIView?
    @IBOutlet weak var infoView1: UIView?
    @IBOutlet weak var infoView2: UIView?
    @IBOutlet weak var name_label1 : UILabel?
    @IBOutlet weak var name_label2 : UILabel?
    @IBOutlet weak var bloodGroup_label1 : UILabel?
    @IBOutlet weak var bloodGroup_label2 : UILabel?
    @IBOutlet weak var title_label1 : UILabel?
    @IBOutlet weak var title_label2 : UILabel?
    
    @IBOutlet weak var mobile1_label : UILabel?
    @IBOutlet weak var mobile2_label : UILabel?
    @IBOutlet weak var mobile3_label : UILabel?
    
    @IBOutlet weak var mobile1_label_View2 : UILabel?
    @IBOutlet weak var mobile2_label_View2 : UILabel?
    @IBOutlet weak var mobile3_label_View2 : UILabel?
    
    @IBOutlet weak var name1_label : UILabel?
    @IBOutlet weak var name2_label : UILabel?
    @IBOutlet weak var name3_label : UILabel?
    
    @IBOutlet weak var relation1_label : UILabel?
    @IBOutlet weak var relation2_label : UILabel?
    @IBOutlet weak var relation3_label : UILabel?
    
    @IBOutlet weak var checkBoxButton1 : UIButton?
    @IBOutlet weak var checkBoxButton2 : UIButton?
    
    var contactsArray : NSMutableArray = []

    var appConstants : AppConstants = AppConstants()
    
    
    override func viewWillAppear(_ animated: Bool) {
        name_label1?.text = appConstants.defaults.value(forKey: "userName") as? String
        name_label2?.text = appConstants.defaults.value(forKey: "userName") as? String
        let bloodGroup : String = (appConstants.defaults.value(forKey: "userBloodGroup") as? String)!
        bloodGroup_label1?.text = "MY BLOOD GROUP: \(bloodGroup)"
        bloodGroup_label2?.text = "MY BLOOD GROUP: \(bloodGroup)"
        title_label1?.text = appConstants.defaults.value(forKey: "infoLabel") as? String
        title_label2?.text = appConstants.defaults.value(forKey: "infoLabel") as? String
        
        name1_label?.text = appConstants.defaults.value(forKey: "contact1Name") as? String
        name2_label?.text = appConstants.defaults.value(forKey: "contact2Name") as? String
        name3_label?.text = appConstants.defaults.value(forKey: "contact3Name") as? String
        
         mobile1_label?.text = appConstants.defaults.value(forKey: "contact1Mobile") as? String
         mobile2_label?.text = appConstants.defaults.value(forKey: "contact2Mobile") as? String
         mobile3_label?.text = appConstants.defaults.value(forKey: "contact3Mobile") as? String
        
        mobile1_label_View2?.text = appConstants.defaults.value(forKey: "contact1Mobile") as? String
        mobile2_label_View2?.text = appConstants.defaults.value(forKey: "contact2Mobile") as? String
        mobile3_label_View2?.text = appConstants.defaults.value(forKey: "contact3Mobile") as? String
        
        relation1_label?.text = appConstants.defaults.value(forKey: "contact1Relation") as? String
        relation2_label?.text = appConstants.defaults.value(forKey: "contact2Relation") as? String
        relation3_label?.text = appConstants.defaults.value(forKey: "contact3Relation") as? String
        
        Name_View1?.backgroundColor = UIColor().HexToColor(hexString: appConstants.nameInfoView_brighterColor).withAlphaComponent(0.7)
        
         Name_View2?.backgroundColor = UIColor().HexToColor(hexString: appConstants.nameInfoView_brighterColor).withAlphaComponent(0.7)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK: Back Button
    @IBAction func Baack(sender : UIButton)
    {
        self.contactsArray.removeAllObjects()
        self.navigationController?.popViewController(animated: true)
    }
    
  //MARK: Check Box 1 Selection
    var CheckBox1Selected = false
    var CheckBox2Selected = false
    @IBAction func CheckBox1Clicked(sender: AnyObject) {
        
        if !CheckBox1Selected {
            let image = UIImage(named: "ic_box_selected") as UIImage!
            checkBoxButton1?.setImage(image, for: .normal)
            
            let image1 = UIImage(named: "ic_box_unselected") as UIImage!
            checkBoxButton2?.setImage(image1, for: .normal)
            print("Check box 1 selected")
            CheckBox1Selected = true
            CheckBox2Selected = false
        } else {
            
            let image = UIImage(named: "ic_box_unselected") as UIImage!
                checkBoxButton1?.setImage(image, for: .normal)
            let image1 = UIImage(named: "ic_box_selected") as UIImage!
            checkBoxButton2?.setImage(image1, for: .normal)
                print("check box 1 unselected")
                checkBoxButton1?.tag = 0
                CheckBox1Selected = false
                CheckBox2Selected = true
            }
        }
    
     //MARK: Check Box 2 Selection
    
    @IBAction func CheckBox2Clicked(sender: AnyObject) {
        
        if !CheckBox2Selected {
            let image = UIImage(named: "ic_box_selected") as UIImage!
            checkBoxButton2?.setImage(image, for: .normal)
            
            let image1 = UIImage(named: "ic_box_unselected") as UIImage!
            checkBoxButton1?.setImage(image1, for: .normal)
            print("Check box 2 selected")
            CheckBox2Selected = true
            CheckBox1Selected = false
        } else {
            
            let image = UIImage(named: "ic_box_unselected") as UIImage!
            checkBoxButton2?.setImage(image, for: .normal)
            
            let image1 = UIImage(named: "ic_box_selected") as UIImage!
            checkBoxButton1?.setImage(image1, for: .normal)

            print("check box 2 unselected")
            CheckBox2Selected = false
            CheckBox1Selected = true
        }
    }
    
    //MARK: Move to Next Page
    @IBAction func NextClicked(sender : UIButton)
    {
       
        print("box 1 \(CheckBox1Selected)")
        print("box 2 \(CheckBox2Selected)")
        appConstants.defaults.set(CheckBox1Selected, forKey: "template1")
        appConstants.defaults.set(CheckBox2Selected, forKey: "template2")
        
        if(CheckBox1Selected) || (CheckBox2Selected)
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ChooseBackgroundVC = storyboard.instantiateViewController(withIdentifier: "ChooseBackgroundVC") as! ChooseBackgroundVC
        self.navigationController?.pushViewController(ChooseBackgroundVC, animated: true)
       }
        else{
           let MessageString = "Please Choose a Template First"
            appConstants.showAlert(title: "Oops", message: MessageString, controller: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
