//
//  DonorListCell.swift
//  saviour
//
//  Created by Pooja on 24/07/17.
//  Copyright © 2017 Truway. All rights reserved.
//

import UIKit

class DonorListCell: UITableViewCell {
    
    @IBOutlet weak var imgView : UIImageView?
    @IBOutlet weak var donorNumberLabel : UILabel?
    @IBOutlet weak var nameLabel : UILabel?
    @IBOutlet weak var mobileLabel : UILabel?
    @IBOutlet weak var bloodGroupLabel : UILabel?
    @IBOutlet weak var cityLabel : UILabel?
    @IBOutlet weak var stateLabel : UILabel?
    @IBOutlet weak var deleteButton : UIButton?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
