//
//  DonorListVC.swift
//  saviour
//
//  Created by Pooja on 24/07/17.
//  Copyright © 2017 Truway. All rights reserved.
//

import UIKit
import ContactsUI

class DonorListVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,CNContactPickerDelegate,apiManagerDelegate{
    
    @IBOutlet weak var headingview : UIView?
    @IBOutlet weak var tableView : UITableView?
    var appConstants : AppConstants = AppConstants()
    var rowCount : Int = 0
    
    @IBOutlet weak var nameTextField : UITextField?
    @IBOutlet weak var mobileTextField : UITextField?
    @IBOutlet weak var bloodGroupTextField : UITextField?
    @IBOutlet weak var cityTextField : UITextField?
    @IBOutlet weak var stateTextField : UITextField?
    @IBOutlet weak var countryTextField : UITextField?
    
    @IBOutlet weak var popUpView : UIView?
    
    var nameString : NSString = ""
    var mobileString : NSString = ""
    var bloodGroupString : NSString = ""
    var cityString : NSString = ""
    var stateString : NSString = ""
    var countryString : NSString = ""
    var donorIdString : NSString = ""
    
    var nameArray : NSMutableArray = []
    var mobileArray : NSMutableArray = []
    var bloodGroupArray : NSMutableArray = []
    var cityArray : NSMutableArray = []
    var stateArray : NSMutableArray = []
    var countryArray : NSMutableArray = []
    var donorIdArray : NSMutableArray = []
    
    var donorlistArray : NSArray = []
    
    var apiManager : ApiManager = ApiManager()
    
    var selectedIndex: Int = 0
    var SelectbloodgroupArray : NSArray = ["Not Sure","A+","A-","B+","B-","AB+","AB-","O+","O-"]
    @IBOutlet weak var tableviewBloodGroup : UITableView?
    @IBOutlet weak var bloodgrouptableShowButton : UIButton?
    
    override func viewWillAppear(_ animated: Bool) {
        
        tableviewBloodGroup?.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        tableviewBloodGroup?.isHidden = true
        popUpView?.isHidden = true
        
        headingview?.backgroundColor =  UIColor().HexToColor(hexString: appConstants.nameInfoView_color).withAlphaComponent(0.7)
       
        getMyDonorListApiCall()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.apiManager = ApiManager()
        apiManager.delegate = self
    }
    
    //MARK: Get blood Group List
    @IBAction func GetBloodGroupListAction()
    {
        tableviewBloodGroup?.isHidden = false
        popUpView?.isHidden = false
        tableviewBloodGroup?.tag = 1
    }
    
    @IBAction func click_Contact(_ sender: UIButton) {
        let cnPicker = CNContactPickerViewController()
        cnPicker.delegate = self
        
        print(sender.tag)
        appConstants.defaults.set(sender.tag as Int, forKey: "tag")
        self.present(cnPicker, animated: true, completion: nil)
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "addNewContact"), object: nil, userInfo: ["contactToAdd": contact])
        
                self.nameTextField?.text = contact.givenName + " " + contact.familyName
                if contact.phoneNumbers.count > 0 {
                    self.mobileTextField?.text = (contact.phoneNumbers[0].value ).value(forKey: "digits") as? String
                }
        // SetUI()
    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        picker.dismiss(animated: true, completion: nil)
        }
    
    //MARK: TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView.tag == 1)
        {
        return SelectbloodgroupArray.count
        }
        else{
            return donorlistArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(tableView.tag == 1)
        {
        let cell = tableviewBloodGroup?.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)
        cell?.contentView.backgroundColor = UIColor.groupTableViewBackground
        cell?.textLabel?.backgroundColor = UIColor.groupTableViewBackground
        cell?.textLabel!.text = SelectbloodgroupArray[indexPath.row] as? String
        cell?.textLabel!.font = UIFont(name: (cell?.textLabel?.font.fontName)!, size:14)
        cell?.textLabel?.textAlignment = NSTextAlignment.center
        return cell!
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell") as! DonorListCell!
                    cell?.donorNumberLabel?.text = "Donor \(indexPath.row + 1)"
                    cell?.nameLabel?.text = nameArray[indexPath.row] as? String
                    cell?.mobileLabel?.text = mobileArray[indexPath.row] as? String
                    selectedIndex = indexPath.row
                   cell?.deleteButton?.tag = indexPath.row
                    cell?.deleteButton?.addTarget(self, action: #selector(deleteDonor), for: .touchUpInside)
            
                        return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if(tableView.tag == 1)
                {
                  bloodGroupTextField?.text = SelectbloodgroupArray[indexPath.row] as? String
                    tableviewBloodGroup?.isHidden = true
                }
                else{
        
                nameTextField?.text = nameArray[indexPath.row] as? String
                mobileTextField?.text = mobileArray[indexPath.row] as? String
                bloodGroupTextField?.text = bloodGroupArray[indexPath.row] as? String
                cityTextField?.text = cityArray[indexPath.row] as? String
                stateTextField?.text = stateArray[indexPath.row] as? String
                countryTextField?.text = countryArray[indexPath.row] as? String
              donorIdString = donorIdArray[indexPath.row] as! String as NSString
        }
    }
//MARK: Back Clicked
    @IBAction func BackClicked(sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ViewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
       self.navigationController?.pushViewController(ViewController, animated: false)
    }
    
    //MARK: SAve Donor
    @IBAction func saveDonor(sender : UIButton)
    {
        nameString = (nameTextField?.text)! as NSString
        mobileString = (mobileTextField?.text)! as NSString
        bloodGroupString = (bloodGroupTextField?.text)! as NSString
        cityString = (cityTextField?.text)! as NSString
        stateString = (stateTextField?.text)! as NSString
        countryString = (countryTextField?.text)!as NSString
        
         if(nameTextField?.text?.trimmingCharacters(in: .whitespaces).isEmpty)! || (mobileTextField?.text?.trimmingCharacters(in: .whitespaces).isEmpty)!
         {
            let MessageString = "Please Provide Donor Name and Mobile"
            appConstants.showAlert(title: "Oops", message: MessageString, controller: self)
        }
         else{
            
            AddMyDonorListApiCall()
//            nameArray.insert(nameString, at: 0)
//            mobileArray.insert(mobileString, at: 0)
//            bloodGroupArray.insert(bloodGroupString, at: 0)
//            cityArray.insert(cityString, at: 0)
//            stateArray.insert(stateString, at: 0)
//            countryArray.insert(countryString, at: 0)
//            rowCount = rowCount + 1
            tableView?.reloadData()
            nameTextField?.text = ""
            mobileTextField?.text = ""
            bloodGroupTextField?.text = ""
            cityTextField?.text = ""
            stateTextField?.text = ""
            countryTextField?.text = ""
        }
    }
    
    //MARK: Add Donor Button
    @IBAction func AddDonorAction(sender : UIButton)
    {
        nameTextField?.text = ""
        mobileTextField?.text = ""
        bloodGroupTextField?.text = ""
        cityTextField?.text = ""
        stateTextField?.text = ""
        countryTextField?.text = ""
    }
    
    //MARK: Delete Donor from List
    @IBAction func deleteDonor(sender : UIButton)
    {
    // tableView?.deleteRows(at: selectedIndex, with: UITableViewRowAnimation.left)
        
        print("delete row \(sender.tag)")
        DeleteMyDonorListApiCall(tag:sender.tag)
      // tableView?.reloadData()
    }
    
    //MARK: Get My Donor List Api Call
    func getMyDonorListApiCall()
    {
        let userId : Int = appConstants.defaults.value(forKey: "userId")as! Int
         appConstants.showLoadingHUD(to_view: self.view)
        self.apiManager.ApiGetMyDonorList(action: "getMyOwnDonorList",userId:userId)
    }
    
    //MARK: Add My Donor List Api Call
    func AddMyDonorListApiCall()
    {
        let userId : Int = appConstants.defaults.value(forKey: "userId")as! Int
        appConstants.showLoadingHUD(to_view: self.view)
        if(donorIdString == "")
        {
        self.apiManager.ApiUpdateMyDonorList(action: "updateMyOwnDonor",userId:userId,donorId: "",donorName : nameString as String,donorMobile: mobileString as String,donorBloodGroup: bloodGroupString as String,donorCity: cityString as String,donorState : stateString as String,donorCountry: countryString as String)
            print("add new donor")
        }
        else{
            self.apiManager.ApiUpdateMyDonorList(action: "updateMyOwnDonor",userId:userId,donorId: donorIdString as String,donorName : nameString as String,donorMobile: mobileString as String,donorBloodGroup: bloodGroupString as String,donorCity: cityString as String,donorState : stateString as String,donorCountry: countryString as String)
            print("update existing donor")
        }
    }
    //MARK: Delete My Donor List Api Call
    func DeleteMyDonorListApiCall(tag: Int)
    {
        let userId : Int = appConstants.defaults.value(forKey: "userId")as! Int
        let donor_id = donorIdArray.object(at: tag)
        appConstants.showLoadingHUD(to_view: self.view)
       
            self.apiManager.ApiDeleteMyDonorList(action: "deleteDonor",userId:userId,donorId: donor_id as! String)
            print("delete donor")
    }
    
    //MARK: API Response call
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
        for (key, value) in response {
             appConstants.hideLoadingHUD(for_view: self.view)
            print("key=\(key), value=\(value)")
            let responseDictionary : NSDictionary = value as! NSDictionary
           if let responseMessage : NSString = responseDictionary .value(forKey: "message") as? NSString
           {
            
            if(responseMessage.isEqual(to: "success"))
            {
            if let listArray : NSArray =  responseDictionary .value(forKey: "donorlist") as? NSArray
            {
                
             donorlistArray  = responseDictionary .value(forKey: "donorlist") as! NSArray
               
                for dict in donorlistArray{
                    print("dict12 \(dict)")
                    let dict1 : NSDictionary = dict as! NSDictionary
                    nameString = dict1.value(forKey: "donorName") as! NSString
                    bloodGroupString = dict1.value(forKey: "bloodgroup") as! NSString
                    mobileString = dict1.value(forKey: "donorMobile") as! NSString
                    cityString = dict1.value(forKey: "donorCity") as! NSString
                    stateString = dict1.value(forKey: "donorState") as! NSString
                    countryString = dict1.value(forKey: "country") as! NSString
                    donorIdString = dict1.value(forKey: "donorId") as! NSString
                    
                    nameArray.insert(nameString, at: 0)
                    mobileArray.insert(mobileString, at: 0)
                    bloodGroupArray.insert(bloodGroupString, at: 0)
                    cityArray.insert(cityString, at: 0)
                    stateArray.insert(stateString, at: 0)
                    countryArray.insert(countryString, at: 0)
                    donorIdArray.insert(donorIdString, at: 0)
                    tableView?.reloadData()
                }
                 tableView?.reloadData()
            }
            else{
                print("donor deleted successfully")
                 getMyDonorListApiCall()
                }
            }
            else{
                tableView?.reloadData()
                print(donorIdArray.count)
                print("response fail from server")
                let MessageString = "No Donor Found"
                 appConstants.showAlert(title: "Oops", message: MessageString, controller: self)
                
                
            }
           }
            else{
                print("donor added successfully")
                getMyDonorListApiCall()
            tableView?.reloadData()
            }
           
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
