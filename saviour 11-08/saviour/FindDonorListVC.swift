//
//  FindDonorListVC.swift
//  saviour
//
//  Created by Pooja on 26/07/17.
//  Copyright © 2017 Truway. All rights reserved.
//

import UIKit

class FindDonorListVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
     @IBOutlet weak var tableView : UITableView?
    var donorListArray : NSArray = []
    var bloodGroupArray : NSMutableArray = []
    var mobileArray : NSMutableArray = []
    var namearray : NSMutableArray = []
    var locationArray : NSMutableArray = []
    
    @IBOutlet weak var headerView : UIView?
    @IBOutlet weak var viewHeadingLabel : UILabel?
    var headerString : NSString = ""
    var appConstants : AppConstants = AppConstants()
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        for dict in donorListArray
        {
            let dict1 : NSDictionary = dict as! NSDictionary
            let name = dict1.value(forKey: "donorName")
            let mobile = dict1.value(forKey: "donorMobile")
            let bloodGroup = dict1.value(forKey: "bloodGroup")
            let location = dict1.value(forKey: "location")
            
            namearray.insert(name!, at: 0)
            mobileArray.insert(mobile!, at: 0)
            bloodGroupArray.insert(bloodGroup!, at: 0)
            locationArray.insert(location!, at: 0)
        }
        
        viewHeadingLabel?.text = headerString as String
        
        namearray = namearray.reversed() as! NSMutableArray
        mobileArray = mobileArray.reversed() as! NSMutableArray
        bloodGroupArray = bloodGroupArray.reversed() as! NSMutableArray
        locationArray = locationArray.reversed() as! NSMutableArray
        
        headerView?.backgroundColor = UIColor().HexToColor(hexString: appConstants.nameInfoView_brighterColor).withAlphaComponent(1.0)
        
        print(namearray.count)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK: TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return donorListArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell") as! SearchListCell!
        
       cell?.nameLabel?.text = namearray[indexPath.row] as? String
        cell?.mobileLabel?.text = mobileArray[indexPath.row] as? String
        cell?.cityLabel?.text = locationArray[indexPath.row] as? String
        cell?.bloodGroupLabel?.text = bloodGroupArray[indexPath.row] as? String
        // cell?.deleteButton?.addTarget(self, action: #selector(deleteDonor), for: .touchUpInside)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
    }
    
    @IBAction func BackClicked(sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 }
