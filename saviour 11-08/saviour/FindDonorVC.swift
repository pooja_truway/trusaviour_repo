//
//  FindDonorVC.swift
//  saviour
//
//  Created by Pooja on 19/07/17.
//  Copyright © 2017 Truway. All rights reserved.
//

import UIKit

class FindDonorVC: UIViewController,UITextFieldDelegate,apiManagerDelegate,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var locationTextField : UITextField?
    @IBOutlet weak var bloodGroupTextField : UITextField?
    @IBOutlet weak var cityTextField : UITextField?
    @IBOutlet weak var stateTextField : UITextField?
    @IBOutlet weak var countryTextField : UITextField?
    
    var locationString : NSString = ""
    var bloodGroupString : NSString = ""
    var cityString : NSString = ""
    var stateString : NSString = ""
    var countryString : NSString = ""
    
    var appConstants : AppConstants  = AppConstants()
    var apiManager : ApiManager = ApiManager()
    @IBOutlet weak var tableview : UITableView?
    @IBOutlet weak var bloodgrouptableShowButton : UIButton?
     var bloodgroupArray : NSArray = ["All","A+","A-","B+","B-","AB+","AB-","O+","O-"]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.apiManager = ApiManager()
        apiManager.delegate = self
        tableview?.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        tableview?.isHidden = true
        
        bloodGroupTextField?.text = "All"

        // Do any additional setup after loading the view.
    }
    //MARK: Back Button
    @IBAction func BackClicked(sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ViewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(ViewController, animated: false)
    }
    
    @IBAction func searchResults(sender: UIButton)
    {
        locationString = (locationTextField?.text)! as NSString
        bloodGroupString = (bloodGroupTextField?.text)! as NSString
        cityString = (cityTextField?.text)! as NSString
        stateString = (stateTextField?.text)! as NSString
        countryString = (countryTextField?.text)!as NSString
        
        FindDonorListApiCall()

    }
    //MARK: Api call to find Donor
func FindDonorListApiCall()
{
    let userId : Int = appConstants.defaults.value(forKey: "userId")as! Int
    appConstants.showLoadingHUD(to_view: self.view)
    
    self.apiManager.ApiFindDonorList(action: "findBloodDonor",userId:userId,location:locationString as String, bloodGroup: bloodGroupString as String,city: cityString as String,state : stateString as String,country: countryString as String)
        print("find donor")
}
    
    //MARK: API Response call
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
        for (key, value) in response {
            appConstants.hideLoadingHUD(for_view: self.view)
            print("key=\(key), value=\(value)")
           if let responseDictionary : NSDictionary = value as? NSDictionary
           {
            
             if let donorList : NSArray = responseDictionary .value(forKey: "donorlist") as? NSArray
             {
            // let donorList : NSArray = (responseDictionary .value(forKey: "donorlist") as? NSArray)!
         
            
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let FindDonorListVC = storyboard.instantiateViewController(withIdentifier: "FindDonorListVC") as! FindDonorListVC
                        FindDonorListVC.donorListArray = donorList
                    FindDonorListVC.headerString = "Blood Donor List"
                        self.navigationController?.pushViewController(FindDonorListVC, animated: true)
            }
          
           else{
             let MessageString = "No Data Found"
             appConstants.showAlert(title: "Oops", message: MessageString, controller: self)
            }
        }
    }
    }
    
    //MARK: Get blood Group List
    @IBAction func GetBloodGroupListAction()
    {
        tableview?.isHidden = false
    }
    //MARK: Hide Table View
    func dismissView() {
        tableview?.isHidden = true
            }
    
    //MARK: TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bloodgroupArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview?.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)
        cell?.contentView.backgroundColor = UIColor.groupTableViewBackground
        cell?.textLabel?.backgroundColor = UIColor.groupTableViewBackground
        cell?.textLabel!.text = "\(bloodgroupArray[indexPath.row])"
        cell?.textLabel!.font = UIFont(name: (cell?.textLabel?.font.fontName)!, size:14)
        cell?.textLabel?.textAlignment = NSTextAlignment.center
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        bloodGroupTextField?.text = bloodgroupArray[indexPath.row] as? String
        tableview?.isHidden = true
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
