//
//  LockScreen2VC.swift
//  saviour
//
//  Created by Pooja on 29/06/17.
//  Copyright © 2017 Truway. All rights reserved.
//

import UIKit

class LockScreen2VC: UIViewController {
    @IBOutlet weak var Wallpaper_Image : UIImageView?
    var selected_Image : UIImage!
    
    @IBOutlet weak var Name_View1 : UIView?
    @IBOutlet weak var title_view1 : UIView?
    
    @IBOutlet weak var infoView1: UIView?
    
    @IBOutlet weak var name_label1 : UILabel?
    @IBOutlet weak var bloodGroup_label1 : UILabel?
    
    @IBOutlet weak var title_label1 : UILabel?
    
    @IBOutlet weak var mobile1_label : UILabel?
    @IBOutlet weak var mobile2_label : UILabel?
    @IBOutlet weak var mobile3_label : UILabel?
    
    @IBOutlet weak var name1_label : UILabel?
    @IBOutlet weak var name2_label : UILabel?
    @IBOutlet weak var name3_label : UILabel?
    
    @IBOutlet weak var relation1_label : UILabel?
    @IBOutlet weak var relation2_label : UILabel?
    @IBOutlet weak var relation3_label : UILabel?
    
    @IBOutlet weak var backButton : UIButton?
    @IBOutlet weak var setButton : UIButton?
    
     @IBOutlet weak var manageHeight : NSLayoutConstraint?
    
    var appConstants : AppConstants = AppConstants()
    
    override func viewWillAppear(_ animated: Bool) {
        Wallpaper_Image?.image = selected_Image
        backButton?.layer.cornerRadius = 15.0
        setButton?.layer.cornerRadius = 15.0
        
        name_label1?.text = appConstants.defaults.value(forKey: "userName") as? String
        
        let bloodGroup : String = (appConstants.defaults.value(forKey: "userBloodGroup") as? String)!
        bloodGroup_label1?.text = "MY BLOOD GROUP: \(bloodGroup)"
        title_label1?.text = appConstants.defaults.value(forKey: "infoLabel") as? String
        
        name1_label?.text = appConstants.defaults.value(forKey: "contact1Name") as? String
        name2_label?.text = appConstants.defaults.value(forKey: "contact2Name") as? String
        name3_label?.text = appConstants.defaults.value(forKey: "contact3Name") as? String
               
        mobile1_label?.text = appConstants.defaults.value(forKey: "contact1Mobile") as? String
        mobile2_label?.text = appConstants.defaults.value(forKey: "contact2Mobile") as? String
        mobile3_label?.text = appConstants.defaults.value(forKey: "contact3Mobile") as? String
       
        
        relation1_label?.text = appConstants.defaults.value(forKey: "contact1Relation") as? String
        relation2_label?.text = appConstants.defaults.value(forKey: "contact2Relation") as? String
        relation3_label?.text = appConstants.defaults.value(forKey: "contact3Relation") as? String
        
    }


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func BackClicked(sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func SetButtonClicked(sender : UIButton)
    {
        backButton?.isHidden = true
        setButton?.isHidden = true
        captureScreen()
    }
    
    func captureScreen() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, UIScreen.main.scale)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        UIImageWriteToSavedPhotosAlbum(image!, nil, nil, nil)
        backButton?.isHidden = false
        setButton?.isHidden = false
        let MessageString = "Go To Photo Gallery And Set This Image As Your Lock Screen"
        appConstants.showAlert(title: "", message: MessageString, controller: self)
        return image
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 }
