//
//  MapVC.swift
//  saviour
//
//  Created by Pooja on 25/07/17.
//  Copyright © 2017 Truway. All rights reserved.
//
import UIKit
import GoogleMaps

class MapVC: UIViewController ,CLLocationManagerDelegate,GMSMapViewDelegate,apiManagerDelegate{
    
    @IBOutlet weak var mapView: GMSMapView!
    var locationArray : NSArray = []
    var getCurrentLocation : Bool = false
    
    let locationManager = CLLocationManager()
    var currentLocation : CLLocationCoordinate2D?
    var apiManager : ApiManager = ApiManager()
    var appConstants : AppConstants = AppConstants()
    
    var HospitalLocationArray : NSMutableArray = []
    var longitudeArray : NSMutableArray = []
    var vicinityArray : NSMutableArray = []
    var HospitalNameArray : NSMutableArray = []
    
    var polyline = GMSPolyline()
    
    override func viewDidLoad()  {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        mapView.delegate = self
        locationManager.startUpdatingLocation()
        
        self.apiManager = ApiManager()
         apiManager.delegate = self
        // self.view = mapView
    }
    
    private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        
        if status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
            mapView.isMyLocationEnabled = true
            mapView.settings.myLocationButton = true
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.first {
            
            locationArray = locations as NSArray
            let MylocationObj = locationArray.lastObject as! CLLocation
            print("last object array \(MylocationObj)")
            let coord = MylocationObj.coordinate
            
            self.locationManager.stopUpdatingLocation()
            
            mapView.camera = GMSCameraPosition(target: coord, zoom: 15, bearing: 0, viewingAngle: 0)
            //viewMap.delegate = self
            
            if(mapView == nil)
            {
                print("nil map")
            }
            print(location.coordinate)
            
            if(getCurrentLocation == false) // to get only one location
            {
                let marker = GMSMarker(position: (MylocationObj.coordinate))
                marker.title = "You are here"
                marker.map = mapView
                //marker.icon =  UIImage(named:"ic_myLocation")
                marker.icon = GMSMarker.markerImage(with: .green)
                currentLocation = MylocationObj.coordinate
                print("current location : \(currentLocation)")
               
                //locationManager.stopUpdatingLocation()
                 self.apiManager.ApiGetTraumaCenters(latitude : MylocationObj.coordinate.latitude,longitude:MylocationObj.coordinate.longitude)
            }
            else{
                
            }
            getCurrentLocation = true
        }
    }
    
    
        func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
    
            let resultArray : NSDictionary = response as NSDictionary
           if let geoLocationArray : NSArray = resultArray.value(forKey: "results") as? NSArray
           {
    
                for dict in geoLocationArray {
                    print("dict:  \(dict)")
    
                    let dict1 = dict as! NSDictionary
                    print(dict1)
    
            let geometrydict = dict1.value(forKey: "geometry")as! NSDictionary
            let locationDict = geometrydict.value(forKey: "location") as? NSDictionary
    
            let latitude = locationDict?.value(forKey: "lat")
            let longitude = locationDict?.value(forKey: "lng")
           let myLocation = CLLocation(latitude: latitude as! CLLocationDegrees, longitude: longitude as! CLLocationDegrees)
              
            print("my location : \(myLocation)")
    
            let dictName = dict1.value(forKey: "name")
            HospitalNameArray.insert(dictName!, at: 0)
            let dictVicinity = dict1.value(forKey: "vicinity")
            vicinityArray.insert(dictVicinity!, at: 0)
            HospitalLocationArray.insert(myLocation,at : 0)
    
            }
            print("HospitalNameArray \(HospitalNameArray)")
            print("vicinityArray\(vicinityArray)")
            print("HospitalLocationArray \(HospitalLocationArray)")
    
            for i in 0..<HospitalLocationArray.count {
                var marker = GMSMarker()
                let locationObj = HospitalLocationArray[i]
                    as! CLLocation
                 marker = GMSMarker(position: (locationObj.coordinate))
    
                marker.title = HospitalNameArray[i] as? String
                marker.snippet = vicinityArray[i] as? String
                marker.map = mapView
    
            }

            }
    
           else{
             let routesArray : NSArray = resultArray.value(forKey: "routes") as! NSArray
    
            for dict in routesArray
                {
                    let route = dict as! NSDictionary
                            print(route)
                    let routeOverviewPolyline = route.value(forKey: "overview_polyline")as! NSDictionary
    
                    let points = routeOverviewPolyline.value(forKey: "points")
    
                    let path = GMSPath.init(fromEncodedPath: points! as! String)
                    polyline = GMSPolyline.init(path: path)
                    polyline.strokeWidth = 5
                    
                            polyline.map = self.mapView
                            print("route: \(route)")
                        }
            }
        }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        print("marker tapped")
        print(marker.position.latitude)
        print(marker.position.longitude)
       
        polyline.map = nil
        
        let myLocation = CLLocation(latitude: marker.position.latitude , longitude: marker.position.longitude )
        print("location : \(myLocation)") //tapped location
        
        
        mapView.camera = GMSCameraPosition(target: currentLocation!, zoom: 16, bearing: 0, viewingAngle: 0)
        
        let getLat: CLLocationDegrees = currentLocation!.latitude
        let getLon: CLLocationDegrees = currentLocation!.longitude//Current Location
        
        let origin = "\(getLat),\(getLon)"
        let destination = "\(myLocation.coordinate.latitude),\(myLocation.coordinate.longitude)"
        
        self.apiManager.ApiDrawRoutes(origin : origin,destination:destination)
        return false
    }
    
   // MARK: back button clicked
        @IBAction func BackClicked(sender : UIButton)
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ViewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            self.navigationController?.pushViewController(ViewController, animated: false)
        }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
