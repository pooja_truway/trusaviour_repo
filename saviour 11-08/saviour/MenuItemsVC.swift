//
//  MenuItemsVC.swift
//  saviour
//
//  Created by Pooja on 18/07/17.
//  Copyright © 2017 Truway. All rights reserved.
//

import UIKit

class MenuItemsVC: UIViewController {
    
    @IBOutlet var menu_View : UIView!
    @IBOutlet var Cancel_btn : UIButton!
    @IBOutlet weak var profile_btn : UIButton?
    @IBOutlet weak var bloodDonorList_btn : UIButton?
    @IBOutlet weak var findDonor_btn : UIButton?
    @IBOutlet weak var sos_btn : UIButton?
    @IBOutlet weak var history_btn : UIButton?
    
    
    //MARK: ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        menu_View.layer.cornerRadius = 10
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK: Cancel Button Action
   @IBAction func cancelButtonClicked(sender: UIButton)
    {    print("cancel pressed")
        
        self.view.removeFromSuperview()
    }
    //MARK: My Profile Action
    @IBAction func profileButtonClicked(sender: UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let MyProfileVC = storyboard.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
        self.navigationController?.pushViewController(MyProfileVC, animated: true)
    }
    //MARK: My Blood Donor List Action
    @IBAction func donor_List_ButtonClicked(sender: UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let DonorListVC = storyboard.instantiateViewController(withIdentifier: "DonorListVC") as! DonorListVC
        self.navigationController?.pushViewController(DonorListVC, animated: true)
   
       
    }
    //MARK: Find Donor Action
    @IBAction func Find_Donor_ButtonClicked(sender: UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let FindDonorVC = storyboard.instantiateViewController(withIdentifier: "FindDonorVC") as! FindDonorVC
        self.navigationController?.pushViewController(FindDonorVC, animated: true)
        
    }
    //MARK: Sos Action
    @IBAction func Sos_ButtonClicked(sender: UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SoSVC = storyboard.instantiateViewController(withIdentifier: "SoSVC") as! SoSVC
        self.navigationController?.pushViewController(SoSVC, animated: true)
    }
    
    //MARK: Organ Donor
    
    @IBAction func OrganDonor(sender: UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let OrganDonorVC = storyboard.instantiateViewController(withIdentifier: "OrganDonorVC") as! OrganDonorVC
        self.navigationController?.pushViewController(OrganDonorVC, animated: true)
    }
    //MARK: History Action
    @IBAction func Map_ButtonClicked(sender: UIButton)
    {    
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let MapVC = storyboard.instantiateViewController(withIdentifier: "MapVC") as! MapVC
        self.navigationController?.pushViewController(MapVC, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
