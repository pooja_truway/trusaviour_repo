//
//  MyProfileVC.swift
//  saviour
//
//  Created by Pooja on 19/07/17.
//  Copyright © 2017 Truway. All rights reserved.
//

import UIKit

class MyProfileVC: UIViewController,UIScrollViewDelegate ,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,apiManagerDelegate{
    
    @IBOutlet weak var view1 : UIView?
    @IBOutlet weak var view2 : UIView?
    
    @IBOutlet weak var tapview : UIView?
    
    @IBOutlet weak var dismissTableViewButton : UIButton?
    
    @IBOutlet weak var DonoteBloodview : UIView?
    @IBOutlet weak var IllnessView : UIView?
    @IBOutlet weak var organDonateView : UIView?
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var saveButton : UIButton?
    @IBOutlet weak var yesButton : UIButton?
    @IBOutlet weak var noButton : UIButton?
    @IBOutlet weak var nameTxtField: UITextField?
    @IBOutlet weak var mobileTxtField : UITextField?
    @IBOutlet weak var bloodGroupTxtField : UITextField?
    
     @IBOutlet weak var locationTxtField: UITextField?
     @IBOutlet weak var cityTxtField: UITextField?
     @IBOutlet weak var stateTxtField: UITextField?
     @IBOutlet weak var countryTxtField: UITextField?
    
    @IBOutlet weak var lastDonatedLabel: UILabel?
    @IBOutlet weak var notsureButton : UIButton?
    @IBOutlet weak var threemonthsButton : UIButton?
    @IBOutlet weak var moremonthsButton : UIButton?
    
    @IBOutlet weak var yesOrganDonorButton : UIButton?
    @IBOutlet weak var noOrganDonorButton : UIButton?
    
    @IBOutlet weak var yesAllergyButton : UIButton?
    @IBOutlet weak var noAllergyButton : UIButton?
    
     @IBOutlet weak var diseaseLabel : UILabel?
    @IBOutlet weak var disease1Button : UIButton?
    @IBOutlet weak var disease2Button : UIButton?
     @IBOutlet weak var disease3Button : UIButton?
     @IBOutlet weak var disease4Button : UIButton?
     @IBOutlet weak var disease5Button : UIButton?
    @IBOutlet weak var disease6Button : UIButton?
    @IBOutlet weak var disease7Button : UIButton?
    @IBOutlet weak var disease8Button : UIButton?
    @IBOutlet weak var disease9Button : UIButton?
    @IBOutlet weak var disease10Button : UIButton?
    
    
    @IBOutlet weak var OD1Button : UIButton?
    @IBOutlet weak var OD2Button : UIButton?
    @IBOutlet weak var OD3Button : UIButton?
    @IBOutlet weak var OD4Button : UIButton?
    @IBOutlet weak var OD5Button : UIButton?
    @IBOutlet weak var OD6Button : UIButton?
    @IBOutlet weak var OD7Button : UIButton?
    
    @IBOutlet weak var allergyTextField : UITextField?
    @IBOutlet weak var allergyview : UIView?
    
    var tap: UITapGestureRecognizer?
    
    var bloodgroupArray : NSArray = ["Not Sure","A+","A-","B+","B-","AB+","AB-","O+","O-"]
    @IBOutlet weak var tableview : UITableView?
    @IBOutlet weak var bloodgrouptableShowButton : UIButton?

    @IBOutlet var lastBloodDonatedConstriant : NSLayoutConstraint?
    @IBOutlet var viewHeightConstriant : NSLayoutConstraint?
    
   @IBOutlet var IllnessviewHeightConstriant : NSLayoutConstraint?
   @IBOutlet var OrganviewHeightConstriant : NSLayoutConstraint?
   @IBOutlet var ScrollViewHeightConstriant : NSLayoutConstraint?
    
    var appConstants : AppConstants = AppConstants()
    var apiManager : ApiManager = ApiManager()
    var isReadyToDonateBlood : NSString = "no"
    var duration : NSString = "not sure"
    var allergy : NSString = "no"
    var donateOrgan : NSString = "no"
    
    //Strings
    
    var userName_string : NSString = ""
    var userMobile_string : NSString  = ""
    var  userBloodGroup_string : NSString  = ""
    var location_string : NSString = ""
    var city_string : NSString = ""
    var state_string : NSString = ""
    var country_string : NSString  = ""
    var allergy_string : NSString = ""
    
    
    //MARK: CheckboxButtons
    var YesOrganDonorSelected = false
    var NoOrganDonorSelected = false
    var yesBtn = false
    var noBtn = false
    
    //MARK: illness checkbox
    var b1 = false
    var b2 = false
    var b3 = false
    var b4 = false
    var b5 = false
    var b6 = false
    var b7 = false
    var button8Clicked = false
    var b9 = false
    var b10 = false
    
    var o1 = false
    var o2 = false
    var o3 = false
    var o4 = false
    var o5 = false
    var o6 = false
    var o7 = false

    
    var diseaseString : String = ""
    var organString : String = ""
    
    
    //MARK: ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        
    tableview?.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        tableview?.isHidden = true
        
    view2?.backgroundColor = UIColor().HexToColor(hexString: appConstants.nameInfoView_color).withAlphaComponent(1.0)
    
    view1?.backgroundColor = UIColor().HexToColor(hexString: appConstants.nameInfoView_brighterColor).withAlphaComponent(1.0)
        
    lastDonatedLabel?.isHidden = true
    notsureButton?.isHidden = true
    threemonthsButton?.isHidden = true
    moremonthsButton?.isHidden = true
    DonoteBloodview?.isHidden = true
        IllnessView?.isHidden = true
        organDonateView?.isHidden = true
        diseaseLabel?.isHidden = true
        organDonateView?.isHidden = true
       
    lastBloodDonatedConstriant?.constant = 10
    viewHeightConstriant?.constant = 0
    ScrollViewHeightConstriant?.constant = 500
    OrganviewHeightConstriant?.constant = 0
        
        self.scrollview?.delegate = self
        
        self.scrollview.contentSize = CGSize(width: self.view.frame.size.width, height: 2000)
     saveButton?.layer.cornerRadius = 5.0
    nameTxtField?.text = appConstants.defaults.value(forKey: "userName") as? String
    mobileTxtField?.text = appConstants.defaults.value(forKey: "userMobile") as? String
    bloodGroupTxtField?.text = appConstants.defaults.value(forKey: "userBloodGroup") as? String
        
    locationTxtField?.text = appConstants.defaults.value(forKey: "location") as? String
    stateTxtField?.text = appConstants.defaults.value(forKey: "state") as? String
    cityTxtField?.text = appConstants.defaults.value(forKey: "city") as? String
    countryTxtField?.text = appConstants.defaults.value(forKey: "country") as? String
    allergyTextField?.text = appConstants.defaults.value(forKey: "medicines") as? String
        if let bloodDonateBool : NSString = appConstants.defaults.value(forKey: "isReadyToDonateBlood") as? NSString
        {
        if(bloodDonateBool == "no")
        {
            let image = UIImage(named: "radio_selected") as UIImage!
             let image1 = UIImage(named: "radio_unselected") as UIImage!
            noButton?.setImage(image, for: .normal)
            yesButton?.setImage(image1, for: .normal)
            IllnessView?.isHidden = true
            IllnessviewHeightConstriant?.constant = 0
            appConstants.defaults.set(bloodDonateBool, forKey: "isReadyToDonateBlood")
            NoSelected = true
        }
        else{
            let image = UIImage(named: "radio_selected") as UIImage!
            let image1 = UIImage(named: "radio_unselected") as UIImage!
            yesButton?.setImage(image, for: .normal)
            noButton?.setImage(image1, for: .normal)
            YesSelected = true
            DonoteBloodview?.isHidden = false
            lastDonatedLabel?.isHidden = false
            notsureButton?.isHidden = false
            threemonthsButton?.isHidden = false
            moremonthsButton?.isHidden = false
            lastBloodDonatedConstriant?.constant = 217
            viewHeightConstriant?.constant = 226
            ScrollViewHeightConstriant?.constant = 700
              appConstants.defaults.set(bloodDonateBool, forKey: "isReadyToDonateBlood")
        }
            
        }
       if let duration : NSString = appConstants.defaults.value(forKey: "duration")as? NSString
       {
        if(duration == "not sure")
        {
            let image = UIImage(named: "radio_selected") as UIImage!
            notsureButton?.setImage(image, for: .normal)
            let image1 = UIImage(named: "radio_unselected") as UIImage!
            threemonthsButton?.setImage(image1, for: .normal)
            moremonthsButton?.setImage(image1, for: .normal)
           
        }
        else if (duration == "less than 3 months")
        {
            let image = UIImage(named: "radio_selected") as UIImage!
            let image1 = UIImage(named: "radio_unselected") as UIImage!
            notsureButton?.setImage(image1, for: .normal)
            threemonthsButton?.setImage(image, for: .normal)
            moremonthsButton?.setImage(image1, for: .normal)
        }
        else{
            let image = UIImage(named: "radio_selected") as UIImage!
            let image1 = UIImage(named: "radio_unselected") as UIImage!
            
            notsureButton?.setImage(image1, for: .normal)
            threemonthsButton?.setImage(image1, for: .normal)
            moremonthsButton?.setImage(image, for: .normal)
        }
    }
        
        if let illness : Bool = appConstants.defaults.value(forKey: "illnessSelected")as? Bool
        {
            let bloodDonateBool : NSString = (appConstants.defaults.value(forKey: "isReadyToDonateBlood") as? NSString)!
             if(bloodDonateBool == "yes")
             {
            if (illness == true)
            {
               yesBtn = true
                let image = UIImage(named: "radio_selected") as UIImage!
                yesAllergyButton?.setImage(image, for: .normal)
                
                let image1 = UIImage(named: "radio_unselected") as UIImage!
                noAllergyButton?.setImage(image1, for: .normal)
                print("Check box 1 selected")
                allergyview?.isHidden = true
                
                lastBloodDonatedConstriant?.constant = 570 //217
                IllnessviewHeightConstriant?.constant = 210
                viewHeightConstriant?.constant = 226
                if(YesOrganDonorSelected == true)
                {
                    ScrollViewHeightConstriant?.constant = 1300
                }
                else{
                    ScrollViewHeightConstriant?.constant = 1000
                }
                allergy = "yes"
                IllnessView?.isHidden = false
                diseaseLabel?.isHidden = false
                appConstants.defaults.set(illness, forKey: "illnessSelected")
            }
            else{
                yesBtn = false
            }
            }
        }
        else{
            yesBtn = false
            IllnessviewHeightConstriant?.constant = 0
            IllnessView?.isHidden = true
        }// for any illness or allergies
        
        if let donateOrgan : Bool = appConstants.defaults.value(forKey: "organDonateSelected")as? Bool
        {
            if(donateOrgan == true)
            {
                YesOrganDonorSelected = true
                let image = UIImage(named: "radio_selected") as UIImage!
                yesOrganDonorButton?.setImage(image, for: .normal)
                
                let image1 = UIImage(named: "radio_unselected") as UIImage!
                noOrganDonorButton?.setImage(image1, for: .normal)
                print("Check box 1 selected")
                YesOrganDonorSelected = true
                organDonateView?.isHidden = false
                OrganviewHeightConstriant?.constant = 210
                 appConstants.defaults.set(donateOrgan, forKey: "organDonateSelected")
                
                if(yesBtn == true)
                {
                    ScrollViewHeightConstriant?.constant = 1400
                }
                else{
                    ScrollViewHeightConstriant?.constant = 1100
                }
            }
            else{
                YesOrganDonorSelected = false
                 appConstants.defaults.set(donateOrgan, forKey: "organDonateSelected")
            }
        }
        else{
             YesOrganDonorSelected = false
        }
        
        if let d1 : Bool = appConstants.defaults.value(forKey: "disease1")as? Bool
        {
            if(d1 == true)
            {
                let image = UIImage(named: "ic_box_selected") as UIImage!
                disease1Button?.setImage(image, for: .normal)
            }
            else{
                let image = UIImage(named: "ic_box_unselected") as UIImage!
                disease1Button?.setImage(image, for: .normal)
            }
        }
        
        if let d2 : Bool = appConstants.defaults.value(forKey: "disease2")as? Bool
        {
            if(d2 == true)
            {
                let image = UIImage(named: "ic_box_selected") as UIImage!
                disease2Button?.setImage(image, for: .normal)
            }
            else{
                let image = UIImage(named: "ic_box_unselected") as UIImage!
                disease2Button?.setImage(image, for: .normal)
            }
        }
        
        if let d3 : Bool = appConstants.defaults.value(forKey: "disease3")as? Bool
        {
            if(d3 == true)
            {
                let image = UIImage(named: "ic_box_selected") as UIImage!
                disease3Button?.setImage(image, for: .normal)
            }
            else{
                let image = UIImage(named: "ic_box_unselected") as UIImage!
                disease3Button?.setImage(image, for: .normal)
            }
        }
        
        if let d4 : Bool = appConstants.defaults.value(forKey: "disease4")as? Bool
        {
            if(d4 == true)
            {
                let image = UIImage(named: "ic_box_selected") as UIImage!
                disease4Button?.setImage(image, for: .normal)
            }
            else{
                let image = UIImage(named: "ic_box_unselected") as UIImage!
                disease4Button?.setImage(image, for: .normal)
            }
        }
        
        if let d5 : Bool = appConstants.defaults.value(forKey: "disease1")as? Bool
        {
            if(d5 == true)
            {
                let image = UIImage(named: "ic_box_selected") as UIImage!
                disease5Button?.setImage(image, for: .normal)
            }
            else{
                let image = UIImage(named: "ic_box_unselected") as UIImage!
                disease5Button?.setImage(image, for: .normal)
            }
        }
        
        if let d6 : Bool = appConstants.defaults.value(forKey: "disease6")as? Bool
        {
            if(d6 == true)
            {
                let image = UIImage(named: "ic_box_selected") as UIImage!
                disease6Button?.setImage(image, for: .normal)
            }
            else{
                let image = UIImage(named: "ic_box_unselected") as UIImage!
                disease6Button?.setImage(image, for: .normal)
            }
        }
        
        if let d7 : Bool = appConstants.defaults.value(forKey: "disease7")as? Bool
        {
            if(d7 == true)
            {
                let image = UIImage(named: "ic_box_selected") as UIImage!
                disease7Button?.setImage(image, for: .normal)
            }
            else{
                let image = UIImage(named: "ic_box_unselected") as UIImage!
                disease7Button?.setImage(image, for: .normal)
            }
        }
        
        if let d8 : Bool = appConstants.defaults.value(forKey: "disease8")as? Bool
        {
            if(d8 == true)
            {
                let image = UIImage(named: "ic_box_selected") as UIImage!
                disease8Button?.setImage(image, for: .normal)
            }
            else{
                let image = UIImage(named: "ic_box_unselected") as UIImage!
                disease8Button?.setImage(image, for: .normal)
            }
        }
        
        if let d9 : Bool = appConstants.defaults.value(forKey: "disease9")as? Bool
        {
            if(d9 == true)
            {
                let image = UIImage(named: "ic_box_selected") as UIImage!
                disease9Button?.setImage(image, for: .normal)
            }
            else{
                let image = UIImage(named: "ic_box_unselected") as UIImage!
                disease9Button?.setImage(image, for: .normal)
            }
        }
        
        if let d10 : Bool = appConstants.defaults.value(forKey: "disease10")as? Bool
        {
            if(d10 == true)
            {
                let image = UIImage(named: "ic_box_selected") as UIImage!
                disease10Button?.setImage(image, for: .normal)
                allergyTextField?.isHidden = false
            }
            else{
                let image = UIImage(named: "ic_box_unselected") as UIImage!
                disease10Button?.setImage(image, for: .normal)
                allergyTextField?.isHidden = true
            }
        }
        
        if let or1 : Bool = appConstants.defaults.value(forKey: "organ1")as? Bool
        {
            if(or1 == true)
            {
                let image = UIImage(named: "ic_box_selected") as UIImage!
                OD1Button?.setImage(image, for: .normal)
            }
            else{
                let image = UIImage(named: "ic_box_unselected") as UIImage!
                OD1Button?.setImage(image, for: .normal)
            }
        }
        if let or2 : Bool = appConstants.defaults.value(forKey: "organ2")as? Bool
        {
            if(or2 == true)
            {
                let image = UIImage(named: "ic_box_selected") as UIImage!
                OD2Button?.setImage(image, for: .normal)
            }
            else{
                let image = UIImage(named: "ic_box_unselected") as UIImage!
                OD2Button?.setImage(image, for: .normal)
            }
        }

        if let or3 : Bool = appConstants.defaults.value(forKey: "organ3")as? Bool
        {
            if(or3 == true)
            {
                let image = UIImage(named: "ic_box_selected") as UIImage!
                OD3Button?.setImage(image, for: .normal)
            }
            else{
                let image = UIImage(named: "ic_box_unselected") as UIImage!
                OD3Button?.setImage(image, for: .normal)
            }
        }

        if let or4 : Bool = appConstants.defaults.value(forKey: "organ4")as? Bool
        {
            if(or4 == true)
            {
                let image = UIImage(named: "ic_box_selected") as UIImage!
                OD4Button?.setImage(image, for: .normal)
            }
            else{
                let image = UIImage(named: "ic_box_unselected") as UIImage!
                OD4Button?.setImage(image, for: .normal)
            }
        }

        if let or5 : Bool = appConstants.defaults.value(forKey: "organ5")as? Bool
        {
            if(or5 == true)
            {
                let image = UIImage(named: "ic_box_selected") as UIImage!
                OD5Button?.setImage(image, for: .normal)
            }
            else{
                let image = UIImage(named: "ic_box_unselected") as UIImage!
                OD5Button?.setImage(image, for: .normal)
            }
        }

        if let or6 : Bool = appConstants.defaults.value(forKey: "organ6")as? Bool
        {
            if(or6 == true)
            {
                let image = UIImage(named: "ic_box_selected") as UIImage!
                OD6Button?.setImage(image, for: .normal)
            }
            else{
                let image = UIImage(named: "ic_box_unselected") as UIImage!
                OD6Button?.setImage(image, for: .normal)
            }
        }

        if let or7 : Bool = appConstants.defaults.value(forKey: "organ7")as? Bool
        {
            if(or7 == true)
            {
                let image = UIImage(named: "ic_box_selected") as UIImage!
                OD7Button?.setImage(image, for: .normal)
            }
            else{
                let image = UIImage(named: "ic_box_unselected") as UIImage!
                OD7Button?.setImage(image, for: .normal)
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.apiManager = ApiManager()
        apiManager.delegate = self
        }
    
    //MARK: Back Button
    @IBAction func BackClicked(sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ViewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(ViewController, animated: false)
    }
    
    //MARK: Radio Button 1 Selection
    var YesSelected = false
    var NoSelected = false
    @IBAction func YesButtonClicked(sender: AnyObject) {
        
        if !YesSelected {
            let image = UIImage(named: "radio_selected") as UIImage!
            yesButton?.setImage(image, for: .normal)
            
            let image1 = UIImage(named: "radio_unselected") as UIImage!
            noButton?.setImage(image1, for: .normal)
            DonoteBloodview?.isHidden = false
            lastDonatedLabel?.isHidden = false
            notsureButton?.isHidden = false
            threemonthsButton?.isHidden = false
            moremonthsButton?.isHidden = false
            
            lastBloodDonatedConstriant?.constant =  217
            viewHeightConstriant?.constant = 226
            ScrollViewHeightConstriant?.constant = 700
            print("Check box 1 selected")
            isReadyToDonateBlood = "yes"
            YesSelected = true
            NoSelected = false
        } else {
            
            let image = UIImage(named: "radio_unselected") as UIImage!
            yesButton?.setImage(image, for: .normal)
            let image1 = UIImage(named: "radio_selected") as UIImage!
            noButton?.setImage(image1, for: .normal)
            lastDonatedLabel?.isHidden = true
            notsureButton?.isHidden = true
            threemonthsButton?.isHidden = true
            moremonthsButton?.isHidden = true
            DonoteBloodview?.isHidden = true
            
            
            lastBloodDonatedConstriant?.constant = 15
            viewHeightConstriant?.constant = 0
            ScrollViewHeightConstriant?.constant = 500
            print("check box 1 unselected")
            YesSelected = false
            NoSelected = true
        }
    }
    
    //MARK: Radio Button 2 Selection
    
    @IBAction func NoButtonClicked(sender: AnyObject) {
        
        if !NoSelected {
            let image = UIImage(named: "radio_selected") as UIImage!
            noButton?.setImage(image, for: .normal)
            
            let image1 = UIImage(named: "radio_unselected") as UIImage!
            yesButton?.setImage(image1, for: .normal)
             DonoteBloodview?.isHidden = true
            lastBloodDonatedConstriant?.constant = 5
            viewHeightConstriant?.constant = 0
            ScrollViewHeightConstriant?.constant = 500
            
            print("Check box 2 selected")
            isReadyToDonateBlood = "no"
            IllnessView?.isHidden = true
            organDonateView?.isHidden = true
            IllnessviewHeightConstriant?.constant = 0
            OrganviewHeightConstriant?.constant = 0

            NoSelected = true
            YesSelected = false
        } else {
            
            let image = UIImage(named: "radio_unselected") as UIImage!
            noButton?.setImage(image, for: .normal)
            
            let image1 = UIImage(named: "radio_selected") as UIImage!
            yesButton?.setImage(image1, for: .normal)
            lastBloodDonatedConstriant?.constant = 5
           // viewHeightConstriant?.constant = 0
            
             viewHeightConstriant?.constant = 296

            print("check box 2 unselected")
            NoSelected = false
            YesSelected = true
        }
    }
    //MARK: Radio Button Organ Donor 1 Selection
   
    @IBAction func YesOrganDonorButtonClicked(sender: AnyObject) {
        
        if !YesOrganDonorSelected {
            let image = UIImage(named: "radio_selected") as UIImage!
            yesOrganDonorButton?.setImage(image, for: .normal)
            
            let image1 = UIImage(named: "radio_unselected") as UIImage!
            noOrganDonorButton?.setImage(image1, for: .normal)
            print("Check box 1 selected")
            YesOrganDonorSelected = true
            organDonateView?.isHidden = false
            OrganviewHeightConstriant?.constant = 210
            
            if(yesBtn == true)
            {
            ScrollViewHeightConstriant?.constant = 1200
            }
            else{
              ScrollViewHeightConstriant?.constant = 1100
            }
            donateOrgan = "yes"
            appConstants.defaults.set(YesOrganDonorSelected, forKey: "organDonateSelected")
            //NoOrganDonorSelected = false
        } else {
            
            let image = UIImage(named: "radio_unselected") as UIImage!
            yesOrganDonorButton?.setImage(image, for: .normal)
            let image1 = UIImage(named: "radio_selected") as UIImage!
            noOrganDonorButton?.setImage(image1, for: .normal)
            print("check box 1 unselected")
            YesOrganDonorSelected = false
            OrganviewHeightConstriant?.constant = 0
            //NoOrganDonorSelected = true
        }
    }
    
    //MARK: Radio Button  OrganDonor 2 Selection
    @IBAction func NoOrganDonorButtonClicked(sender: AnyObject) {
        
        if !NoOrganDonorSelected {
            let image = UIImage(named: "radio_selected") as UIImage!
            noOrganDonorButton?.setImage(image, for: .normal)
            
            let image1 = UIImage(named: "radio_unselected") as UIImage!
            yesOrganDonorButton?.setImage(image1, for: .normal)
            print("Check box 2 selected")
            NoOrganDonorSelected = true
            donateOrgan = "no"
            organDonateView?.isHidden = true
             OrganviewHeightConstriant?.constant = 0
            if(yesBtn == true)
            {
            ScrollViewHeightConstriant?.constant = 1100
            }
            else{
                ScrollViewHeightConstriant?.constant = 700
            }
            appConstants.defaults.set(YesOrganDonorSelected, forKey: "organDonateSelected")
           // YesOrganDonorSelected = false
        } else {
            
            let image = UIImage(named: "radio_unselected") as UIImage!
            noOrganDonorButton?.setImage(image, for: .normal)
            
            let image1 = UIImage(named: "radio_selected") as UIImage!
            yesOrganDonorButton?.setImage(image1, for: .normal)
            
            print("check box 2 unselected")
            NoOrganDonorSelected = false
           // YesOrganDonorSelected = true
        }
    }
    
    //MARK:Radio button Blood donated time
    var notsureBtn = false
    var within3monthsBtn = false
    var more3monthsBtn = false
    @IBAction func notSureButtonClicked(sender: AnyObject) {
        
        if !notsureBtn {
            let image = UIImage(named: "radio_selected") as UIImage!
            notsureButton?.setImage(image, for: .normal)
            
            let image1 = UIImage(named: "radio_unselected") as UIImage!
            threemonthsButton?.setImage(image1, for: .normal)
            moremonthsButton?.setImage(image1, for: .normal)
            print("Check box 1 selected")
            notsureBtn = true
            duration = "not sure"
            within3monthsBtn = false
            more3monthsBtn = false
        } else {
            
            let image = UIImage(named: "radio_unselected") as UIImage!
            notsureButton?.setImage(image, for: .normal)
            notsureBtn = false
        }
    }
    
    @IBAction func within3monthsButtonClicked(sender: AnyObject) {
        
        if !within3monthsBtn {
            let image = UIImage(named: "radio_selected") as UIImage!
            threemonthsButton?.setImage(image, for: .normal)
            
            let image1 = UIImage(named: "radio_unselected") as UIImage!
            notsureButton?.setImage(image1, for: .normal)
            moremonthsButton?.setImage(image1, for: .normal)
            print("Check box 1 selected")
            notsureBtn = false
            within3monthsBtn = true
            duration = "less than 3 months"
            more3monthsBtn = false
        } else {
            
            let image = UIImage(named: "radio_unselected") as UIImage!
            threemonthsButton?.setImage(image, for: .normal)
            within3monthsBtn = false
        }
    }
    
    @IBAction func moremonthsButtonClicked(sender: AnyObject) {
        
        if !more3monthsBtn {
            let image = UIImage(named: "radio_selected") as UIImage!
            moremonthsButton?.setImage(image, for: .normal)
            
            let image1 = UIImage(named: "radio_unselected") as UIImage!
            notsureButton?.setImage(image1, for: .normal)
            threemonthsButton?.setImage(image1, for: .normal)
            print("Check box 1 selected")
            more3monthsBtn = true
            within3monthsBtn = false
            duration = "more than 3 months"
            notsureBtn = false
        } else {
            
            let image = UIImage(named: "radio_unselected") as UIImage!
            threemonthsButton?.setImage(image, for: .normal)
            more3monthsBtn = false
        }
    }

    //MARK: allegies radio button click
    
    @IBAction func yesClicked(sender: AnyObject) {
        
        if !yesBtn {
            let image = UIImage(named: "radio_selected") as UIImage!
            yesAllergyButton?.setImage(image, for: .normal)
            
            let image1 = UIImage(named: "radio_unselected") as UIImage!
            noAllergyButton?.setImage(image1, for: .normal)
            print("Check box 1 selected")
            allergyview?.isHidden = true

            lastBloodDonatedConstriant?.constant = 570 //217
            IllnessviewHeightConstriant?.constant = 210
            if(YesOrganDonorSelected == true)
            {
            ScrollViewHeightConstriant?.constant = 1200
            }
            else{
                ScrollViewHeightConstriant?.constant = 1000
            }
            allergy = "yes"
            
            IllnessView?.isHidden = false
            diseaseLabel?.isHidden = false
//            disease1Button?.isHidden = false
//            disease2Button?.isHidden = false
//            disease3Button?.isHidden = false
//            disease4Button?.isHidden = false
//            disease5Button?.isHidden = false
            
           appConstants.defaults.set(yesBtn, forKey: "illnessSelected")
            yesBtn = true
            noBtn = false
            
        } else {
            let image = UIImage(named: "radio_unselected") as UIImage!
            yesAllergyButton?.setImage(image, for: .normal)
            yesBtn = false
            noBtn = true
        }
    }
    
    @IBAction func noClicked(sender: AnyObject) {
        
        if !noBtn {
            let image = UIImage(named: "radio_selected") as UIImage!
            noAllergyButton?.setImage(image, for: .normal)
            
           let image1 = UIImage(named: "radio_unselected") as UIImage!
            yesAllergyButton?.setImage(image1, for: .normal)
           // moremonthsButton?.setImage(image1, for: .normal)
            print("Check box 1 selected")
            
            lastBloodDonatedConstriant?.constant = 190
            noBtn = true
            allergy = "no"
            yesBtn = false
            IllnessView?.isHidden = true
            IllnessviewHeightConstriant?.constant = 0
            
            if(YesOrganDonorSelected == true)
            {
                ScrollViewHeightConstriant?.constant = 900
            }
            else{
                ScrollViewHeightConstriant?.constant = 600
            }
           
            diseaseLabel?.isHidden = true
//            disease1Button?.isHidden = true
//            disease2Button?.isHidden = true
//            disease3Button?.isHidden = true
//            disease4Button?.isHidden = true
//            disease5Button?.isHidden = true
            
            appConstants.defaults.set(yesBtn, forKey: "illnessSelected")

            } else {
            
            let image = UIImage(named: "radio_unselected") as UIImage!
            noAllergyButton?.setImage(image, for: .normal)
            
            noBtn = false
            yesBtn = true
        }
    }
//MARK: Get blood Group List
@IBAction func GetBloodGroupListAction()
    {
        tableview?.isHidden = false
        tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissView))
        self.tapview?.addGestureRecognizer(tap!)
    }
  //MARK: Hide Table View
    func dismissView() {
        tableview?.isHidden = true
        self.tapview?.removeGestureRecognizer(tap!)
    }
    
//MARK: TableView Delegates
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bloodgroupArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview?.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)
        cell?.contentView.backgroundColor = UIColor.groupTableViewBackground
        cell?.textLabel?.backgroundColor = UIColor.groupTableViewBackground
        cell?.textLabel!.text = "\(bloodgroupArray[indexPath.row])"
        cell?.textLabel!.font = UIFont(name: (cell?.textLabel?.font.fontName)!, size:14)
        cell?.textLabel?.textAlignment = NSTextAlignment.center
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        bloodGroupTxtField?.text = bloodgroupArray[indexPath.row] as? String
        self.view.removeGestureRecognizer(tap!)
        tableview?.isHidden = true
    }
    
    //MARK: Save Button Clicked
    @IBAction func saveProfileInfo(sender : UIButton)
    {
        userName_string = (nameTxtField?.text)! as NSString
       userMobile_string = (mobileTxtField?.text)! as NSString
        userBloodGroup_string = (bloodGroupTxtField?.text)! as NSString
      location_string = (locationTxtField?.text)! as String as NSString
      city_string = (cityTxtField?.text)! as String as NSString
      state_string = (stateTxtField?.text)! as String as NSString
      country_string = (countryTxtField?.text)! as String as NSString
       allergy_string = (allergyTextField?.text)! as String as NSString
        print("isReadyToDonateBlood: \(isReadyToDonateBlood)")
        print("duration : \(duration)")
        print("allergy: \(allergy)")
        print("donateOrgan : \(donateOrgan)")
        print("medicines: \(allergy_string)")
        if(nameTxtField?.text?.trimmingCharacters(in: .whitespaces).isEmpty)! || (mobileTxtField?.text?.trimmingCharacters(in: .whitespaces).isEmpty)! || (bloodGroupTxtField?.text?.trimmingCharacters(in: .whitespaces).isEmpty)!
        {
           let MessageString = "Please Provide User Name/Mobile/BloodGroup To Help Someone Save You"
            appConstants.showAlert(title: "Oops", message: MessageString, controller: self)
        }
        else if  (userMobile_string != "") && (validate(value: userMobile_string as String) == false)
        {
           let MessageString = "Please Enter Valid Mobile Number"
            appConstants.showAlert(title: "Oops", message: MessageString, controller: self)
        }
        else{
        appConstants.defaults.set(userName_string, forKey: "userName")
        appConstants.defaults.set(userMobile_string, forKey: "userMobile")
        appConstants.defaults.set(userBloodGroup_string, forKey: "userBloodGroup")
            
        appConstants.defaults.set(location_string, forKey: "location")
        appConstants.defaults.set(city_string,forKey: "city")
        appConstants.defaults.set(state_string, forKey: "state")
        appConstants.defaults.set(country_string, forKey: "country")
        appConstants.defaults.set(duration, forKey: "duration")
        appConstants.defaults.set(allergy_string, forKey: "medicines")
        appConstants.defaults.set(isReadyToDonateBlood, forKey: "isReadyToDonateBlood")
            print("status \(YesSelected)")
        appConstants.defaults.set(allergy, forKey: "allergy")
        appConstants.defaults.set(donateOrgan, forKey: "donateOrgan")
            
            appConstants.defaults.set(b1, forKey: "disease1")
            appConstants.defaults.set(b2, forKey: "disease2")
            appConstants.defaults.set(b3, forKey: "disease3")
            appConstants.defaults.set(b4, forKey: "disease4")
            appConstants.defaults.set(b5, forKey: "disease5")
            appConstants.defaults.set(b6, forKey: "disease6")
            appConstants.defaults.set(b7, forKey: "disease7")
            appConstants.defaults.set(button8Clicked, forKey: "disease8")
            appConstants.defaults.set(b9, forKey: "disease9")
            appConstants.defaults.set(b10, forKey: "disease10")
            
            appConstants.defaults.set(o1, forKey: "organ1")
            appConstants.defaults.set(o2, forKey: "organ2")
            appConstants.defaults.set(o3, forKey: "organ3")
            appConstants.defaults.set(o4, forKey: "organ4")
            appConstants.defaults.set(o5, forKey: "organ5")
            appConstants.defaults.set(o6, forKey: "organ6")
            appConstants.defaults.set(o7, forKey: "organ7")
            
            
            appConstants.defaults.set(yesBtn, forKey: "illnessSelected")
            appConstants.defaults.set(YesOrganDonorSelected, forKey: "organDonateSelected")
            
        sendInfoToServer() // Send All to Server
        }
        
    }
    //MARK: To validate mobile number
    func validate(value: String) -> Bool {
        let PHONE_REGEX = "^[0-9]{10,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        print("validate mobile result \(result)")
        return result
    }
    
    //MARK: Send Info To Server
    func sendInfoToServer()
    {
       let userId : Int = appConstants.defaults.value(forKey: "userId")as! Int
        
        checkSelectedOptions()
        
        allergy_string =  (allergyTextField?.text)! as NSString
        self.apiManager.ApiSendAdditionalDetailsToServer(action: "saveAdditionalDetails",userName:
            
            userMobile_string as String,userMobile: userMobile_string as String,userBloodGroup :userBloodGroup_string as String,userId:userId,location: location_string as String,city: city_string as String,state: state_string as String,country: country_string as String,isReadyToDonateBlood: isReadyToDonateBlood as String,duration: duration as String,alergy: allergy as String,diseases: diseaseString as String,donateOrgan: donateOrgan as String,organList: organString as String)
    }
    
    //MARK: API Response call
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
        for (key, value) in response {
            print("key=\(key), value=\(value)")
            
            let responseDictionary : NSDictionary = value as! NSDictionary
            let responseMessage : NSString = responseDictionary .value(forKey: "message") as! NSString
            if(responseMessage.isEqual(to: "success"))
            {
               
            }
            else{
                
            }
        }
    }
    
    //MARK: Check the selected check box to send details to server
   func checkSelectedOptions()
{
    if(b1 == true)
    {
      let d1String  = disease1Button?.titleLabel?.text
        diseaseString = d1String!
    }
    if(b2 == true)
    {
        let d2String  = disease2Button?.titleLabel?.text
        diseaseString += ",\(d2String!)"
    }
    if(b3 == true)
    {
        let d3String = disease3Button?.titleLabel?.text
        diseaseString += ",\(d3String!)"
    }
    if(b4 == true)
    {
        let d4String = disease4Button?.titleLabel?.text
        diseaseString += ",\(d4String!)"
           }
    if(b5 == true)
    {
        let d5String = disease5Button?.titleLabel?.text
        diseaseString += ",\(d5String!)"
    }
    if(b6 == true)
    {
        let d6String = disease6Button?.titleLabel?.text
       diseaseString += ",\(d6String!)"
    }
    if(b7 == true)
    {
        let d7String = disease7Button?.titleLabel?.text
        diseaseString += ",\(d7String!)"
    }
    if(button8Clicked == true)
    {
        let d8String = disease8Button?.titleLabel?.text
        diseaseString += ",\(d8String!)"
    }
    if(b9 == true)
    {
        let d9String = disease9Button?.titleLabel?.text
        diseaseString += ",\(d9String!)"
    }
    if(b10 == true)
    {
        let d10String = allergyTextField?.text
        diseaseString += ",\(d10String!)"
    }
    print("disease String :\(diseaseString)")
    
    if(o1 == true)
    {
        let o1String  = OD1Button?.titleLabel?.text
        organString = o1String!
    }
    if(o2 == true)
    {
        let o2String  = OD2Button?.titleLabel?.text
         organString += ",\(o2String!)"
    }
    if(o3 == true)
    {
        let o3String  = OD3Button?.titleLabel?.text
        organString += ",\(o3String!)"
    }
    if(o4 == true)
    {
        let o4String  = OD4Button?.titleLabel?.text
        organString += ",\(o4String!)"
    }
    if(o5 == true)
    {
        let o5String  = OD5Button?.titleLabel?.text
        organString += ",\(o5String!)"
    }
    if(o6 == true)
    {
        let o6String  = OD6Button?.titleLabel?.text
        organString += ",\(o6String!)"
    }
    if(o7 == true)
    {
        let o7String  = OD7Button?.titleLabel?.text
        organString += ",\(o7String!)"
    }
    
    print("organ string :\(organString)")
}
    
  //MARK: button1 Selected
    @IBAction func b1Clicked(sender: AnyObject) {
        
        if !b1 {
            let image = UIImage(named: "ic_box_selected") as UIImage!
            disease1Button?.setImage(image, for: .normal)
            appConstants.defaults.set("1", forKey: "disease1")
            print("Check box 1 selected")
            b1 = true
           
        } else {
            let image = UIImage(named: "ic_box_unselected") as UIImage!
            disease1Button?.setImage(image, for: .normal)
            appConstants.defaults.set("0", forKey: "disease1")
            b1 = false
        }
    }
    
    //MARK: button2 Selected
    @IBAction func b2Clicked(sender: AnyObject) {
        
        if !b2 {
            let image = UIImage(named: "ic_box_selected") as UIImage!
            disease2Button?.setImage(image, for: .normal)
            print("Check box 2 selected")
            appConstants.defaults.set("1", forKey: "disease2")
            b2 = true
            
        } else {
            let image = UIImage(named: "ic_box_unselected") as UIImage!
            disease2Button?.setImage(image, for: .normal)
           appConstants.defaults.set("0", forKey: "disease2")
            b2 = false
        }
    }
    
    //MARK: button3 Selected
    @IBAction func b3Clicked(sender: AnyObject) {
        
        if !b3 {
            let image = UIImage(named: "ic_box_selected") as UIImage!
            disease3Button?.setImage(image, for: .normal)
            print("Check box 3 selected")
            appConstants.defaults.set("1", forKey: "disease3")
            b3 = true
            
        } else {
            let image = UIImage(named: "ic_box_unselected") as UIImage!
            disease3Button?.setImage(image, for: .normal)
           appConstants.defaults.set("0", forKey: "disease3")
            b3 = false
        }
    }
    
    //MARK: button4 Selected
    @IBAction func b4Clicked(sender: AnyObject) {
        
        if !b4 {
            let image = UIImage(named: "ic_box_selected") as UIImage!
            disease4Button?.setImage(image, for: .normal)
            print("Check box 4 selected")
             appConstants.defaults.set("1", forKey: "disease4")
            b4 = true
            
        } else {
            let image = UIImage(named: "ic_box_unselected") as UIImage!
            disease4Button?.setImage(image, for: .normal)
            appConstants.defaults.set("0", forKey: "disease4")
            b4 = false
        }
    }

    //MARK: button5 Selected
    @IBAction func b5Clicked(sender: AnyObject) {
        
        if !b5 {
            let image = UIImage(named: "ic_box_selected") as UIImage!
            disease5Button?.setImage(image, for: .normal)
            appConstants.defaults.set("1", forKey: "disease5")
            print("Check box 5 selected")
            b5 = true
            
        } else {
            let image = UIImage(named: "ic_box_unselected") as UIImage!
            disease5Button?.setImage(image, for: .normal)
            appConstants.defaults.set("0", forKey: "disease5")
            b5 = false
        }
    }
    //MARK: button6 Selected
    @IBAction func b6Clicked(sender: AnyObject) {
        
        if !b6 {
            let image = UIImage(named: "ic_box_selected") as UIImage!
            disease6Button?.setImage(image, for: .normal)
            appConstants.defaults.set("1", forKey: "disease6")
            print("Check box 6 selected")
            b6 = true
            
        } else {
            let image = UIImage(named: "ic_box_unselected") as UIImage!
            disease6Button?.setImage(image, for: .normal)
            appConstants.defaults.set("0", forKey: "disease6")
            b6 = false
        }
    }
    //MARK: button7 Selected
    @IBAction func b7Clicked(sender: AnyObject) {
        
        if !b7 {
            let image = UIImage(named: "ic_box_selected") as UIImage!
            disease7Button?.setImage(image, for: .normal)
           appConstants.defaults.set("1", forKey: "disease7")
            print("Check box 7 selected")
            b7 = true
            
        } else {
            let image = UIImage(named: "ic_box_unselected") as UIImage!
            disease7Button?.setImage(image, for: .normal)
            appConstants.defaults.set("0", forKey: "disease7")
            b7 = false
        }
    }
    
    //MARK: button8 Selected
    @IBAction func button8Clicked(sender: AnyObject) {
        
        if !button8Clicked {
            let image = UIImage(named: "ic_box_selected") as UIImage!
            disease8Button?.setImage(image, for: .normal)
            print("Check box 8 selected")
           appConstants.defaults.set("1", forKey: "disease8")
            button8Clicked = true
            
        } else {
            let image = UIImage(named: "ic_box_unselected") as UIImage!
            disease8Button?.setImage(image, for: .normal)
           appConstants.defaults.set("0", forKey: "disease8")
            button8Clicked = false
        }
    }
    
    //MARK: button9 Selected
    @IBAction func b9Clicked(sender: UIButton) {
        
        if !b9 {
            let image = UIImage(named: "ic_box_selected") as UIImage!
            disease9Button?.setImage(image, for: .normal)
            appConstants.defaults.set("1", forKey: "disease9")
            print("Check box 9 selected")
            b9 = true
            
        } else {
            let image = UIImage(named: "ic_box_unselected") as UIImage!
            disease9Button?.setImage(image, for: .normal)
             appConstants.defaults.set("0", forKey: "disease10")
            b9 = false
        }
    }
    
    //MARK: button10 Selected
    @IBAction func b10Clicked(sender: UIButton) {
        
        if !b10 {
            let image = UIImage(named: "ic_box_selected") as UIImage!
            disease10Button?.setImage(image, for: .normal)
            print("Check box 10 selected")
            allergyTextField?.isHidden = false
            b10 = true
            
        } else {
            let image = UIImage(named: "ic_box_unselected") as UIImage!
            disease10Button?.setImage(image, for: .normal)
            allergyTextField?.isHidden = true
            b10 = false
        }
    }
    
    //MARK: button o1 Selected
    @IBAction func o1Clicked(sender: AnyObject) {
        
        if !o1 {
            let image = UIImage(named: "ic_box_selected") as UIImage!
            OD1Button?.setImage(image, for: .normal)
            
            print("Check box o1 selected")
            o1 = true
            
        } else {
            let image = UIImage(named: "ic_box_unselected") as UIImage!
            OD1Button?.setImage(image, for: .normal)
            o1 = false
        }
    }

    //MARK: button o2 Selected
    @IBAction func o2Clicked(sender: AnyObject) {
        
        if !o2 {
            let image = UIImage(named: "ic_box_selected") as UIImage!
            OD2Button?.setImage(image, for: .normal)
            print("Check box o2 selected")
            o2 = true
            
        } else {
            let image = UIImage(named: "ic_box_unselected") as UIImage!
            OD2Button?.setImage(image, for: .normal)
            o2 = false
        }
    }

    //MARK: button o3 Selected
    @IBAction func o3Clicked(sender: AnyObject) {
        
        if !o3 {
            let image = UIImage(named: "ic_box_selected") as UIImage!
            OD3Button?.setImage(image, for: .normal)
            print("Check box o3 selected")
            o3 = true
            
        } else {
            let image = UIImage(named: "ic_box_unselected") as UIImage!
            OD3Button?.setImage(image, for: .normal)
            o3 = false
        }
    }

    //MARK: button o4 Selected
    @IBAction func o4Clicked(sender: AnyObject) {
        
        if !o4 {
            let image = UIImage(named: "ic_box_selected") as UIImage!
            OD4Button?.setImage(image, for: .normal)
            print("Check box o4 selected")
            o4 = true
            
        } else {
            let image = UIImage(named: "ic_box_unselected") as UIImage!
            OD4Button?.setImage(image, for: .normal)
            o4 = false
        }
    }
    //MARK: button o5 Selected
    @IBAction func o5Clicked(sender: AnyObject) {
        
        if !o5 {
            let image = UIImage(named: "ic_box_selected") as UIImage!
            OD5Button?.setImage(image, for: .normal)
            print("Check box o5 selected")
            o5 = true
            
        } else {
            let image = UIImage(named: "ic_box_unselected") as UIImage!
            OD5Button?.setImage(image, for: .normal)
            o5 = false
        }
    }
    //MARK: button o6 Selected
    @IBAction func o6Clicked(sender: AnyObject) {
        
        if !o6 {
            let image = UIImage(named: "ic_box_selected") as UIImage!
            OD6Button?.setImage(image, for: .normal)
            print("Check box o6 selected")
            o6 = true
            
        } else {
            let image = UIImage(named: "ic_box_unselected") as UIImage!
            OD6Button?.setImage(image, for: .normal)
            o6 = false
        }
    }
    
    //MARK: button o7 Selected
    @IBAction func o7Clicked(sender: AnyObject) {
        
        if !o6 {
            let image = UIImage(named: "ic_box_selected") as UIImage!
            OD7Button?.setImage(image, for: .normal)
            print("Check box o7 selected")
            o7 = true
            
        } else {
            let image = UIImage(named: "ic_box_unselected") as UIImage!
            OD7Button?.setImage(image, for: .normal)
            o7 = false
        }
    }

       
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
