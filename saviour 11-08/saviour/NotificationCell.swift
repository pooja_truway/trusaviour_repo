//
//  NotificationCell.swift
//  saviour
//
//  Created by Truway India on 20/07/17.
//  Copyright © 2017 Truway. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    @IBOutlet weak var headingLabel : UILabel?
    @IBOutlet weak var DescriptionLabel : UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
