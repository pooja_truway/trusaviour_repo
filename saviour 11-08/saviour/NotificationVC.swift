//
//  NotificationVC.swift
//  saviour
//
//  Created by Pooja on 20/07/17.
//  Copyright © 2017 Truway. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView : UITableView?
    var notificationArr : NSMutableArray = []
    let delegate = UIApplication.shared.delegate as! AppDelegate
    var headingLabel : NSString = ""
    var descriptionLabel : NSString = ""
    
    var headingArr : NSMutableArray = []
    var descriptionArr : NSMutableArray = []
    
    var appConstants : AppConstants = AppConstants()
    
    var postStoryArr : NSMutableArray = ["ghsgdahgcvhjgdsggsvhsgvhgvhjgdshcghgcdhgcghsghchghcghjdgghdhhhgsghc","sdvgcndsvchvsdhchgdhgjchcghgnbbbbbbbbbghsgdahgcvhjgdsggsvhsgvhgvhjgdshcghgcdhgcghsghchghcghjdgghdhhhgsghc","sdvgcndsvchvsdhchgdhgjchcghgnbbbbbbbbbghsgdahgcvhjgdsggsvhsgvhgvhjgdshcghgcdhgcghsghchghcghjdgghdhhhgsghc","sdvgcndsvchvsdhchgdhgjchcghgnbbbbbbbbbghsgdahgcvhjgdsggsvhsgvhgvhjgdshcghgcdhgcghsghchghcghjdgghdhhhgsghc","frgqsfxgh42561456416w426sdvgcndsvchvsdhchgdhgjchcghgnbbbbbbbbbghsgdahgcvhjgdsggsvhsgvhgvhjgdshcghgcdhgcghsghchghcghjdgghdhhhgsghc76517457623465425646565246532654265324652365625465246256462354653275472346"]
    
    override func viewWillAppear(_ animated: Bool) {
        print("delegate array : \(delegate.notificationArray)")
        notificationArr = delegate.notificationArray
        
        if let notificationArray: NSMutableArray = delegate.notificationArray
        {
        
        print("array count : \(notificationArr.count)")
        for dict in notificationArr
        {
            let apsDict : NSDictionary = notificationArr.object(at: 0) as! NSDictionary
           
            let dict2 : NSDictionary = apsDict.value(forKey: "aps") as! NSDictionary
            print("dict2 : \(dict2)")
            let headingString : NSString = dict2.value(forKey: "heading") as! NSString
            let descriptionString : NSString = dict2.value(forKey: "description") as! NSString
            
            headingArr.insert(headingString, at: 0)
            descriptionArr.insert(descriptionString, at: 0)
        }
            
        }
        else{
            print("array count id 0")
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    // Do any additional setup after loading the view.
    }
    //MARK: TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell") as! NotificationCell!
       // cell?.txtView?.text = postStoryArr[indexPath.row] as! String
        cell?.headingLabel?.text = headingArr[indexPath.row] as? String
        cell?.DescriptionLabel?.text = descriptionArr[indexPath.row] as? String
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
    }
    
    //MARK: Back button clicked
    @IBAction func BackClicked(sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: ClaerAll button clicked
    @IBAction func ClaerAllClicked(sender : UIButton)
    {
        //self.navigationController?.popViewController(animated: true)
        notificationArr.removeAllObjects()
        tableView?.reloadData()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 }
