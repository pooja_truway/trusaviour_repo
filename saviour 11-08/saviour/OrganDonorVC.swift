//
//  OrganDonorVC.swift
//  saviour
//
//  Created by Pooja on 26/07/17.
//  Copyright © 2017 Truway. All rights reserved.
//

import UIKit

class OrganDonorVC: UIViewController,UITextFieldDelegate,apiManagerDelegate,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet weak var locationTextField : UITextField?
    @IBOutlet weak var organTextField : UITextField?
    @IBOutlet weak var bloodGroupTextField : UITextField?
    @IBOutlet weak var cityTextField : UITextField?
    @IBOutlet weak var stateTextField : UITextField?
    @IBOutlet weak var countryTextField : UITextField?
    
    var locationString : NSString = ""
    var bloodGroupString : NSString = ""
    var cityString : NSString = ""
    var stateString : NSString = ""
    var countryString : NSString = ""
    var organString : NSString = ""
    
    var appConstants : AppConstants  = AppConstants()
    var apiManager : ApiManager = ApiManager()
    
    @IBOutlet weak var tableview : UITableView?
    @IBOutlet weak var tableviewOrgan : UITableView?
    @IBOutlet weak var bloodgrouptableShowButton : UIButton?
    @IBOutlet weak var organtableShowButton : UIButton?
    var bloodgroupArray : NSArray = ["All","A+","A-","B+","B-","AB+","AB-","O+","O-"]
    var organListArray : NSArray = ["Heart","Lungs","Kidney","Liver","Intestines","Pancreas","Eyes","Tissues"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.apiManager = ApiManager()
        apiManager.delegate = self
        tableview?.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        tableview?.isHidden = true
        tableviewOrgan?.isHidden = true
        bloodGroupTextField?.text = "All"
        organTextField?.text = "Heart"
        // Do any additional setup after loading the view.
    }
    //MARK: Back Button
    @IBAction func BackClicked(sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ViewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(ViewController, animated: false)
    }
    
    @IBAction func searchResults(sender: UIButton)
    {
        locationString = (locationTextField?.text)! as NSString
        bloodGroupString = (bloodGroupTextField?.text)! as NSString
        cityString = (cityTextField?.text)! as NSString
        stateString = (stateTextField?.text)! as NSString
        countryString = (countryTextField?.text)!as NSString
        organString = (organTextField?.text)! as NSString
        FindOrganDonorListApiCall()
    }
    //MARK: Api call to find Donor
    func FindOrganDonorListApiCall()
    {
        let userId : Int = appConstants.defaults.value(forKey: "userId")as! Int
        appConstants.showLoadingHUD(to_view: self.view)
        
        self.apiManager.ApiFindOrganDonorList(action: "findOrganDonor",userId:userId,location:locationString as String, bloodGroup: bloodGroupString as String,city: cityString as String,state : stateString as String,country: countryString as String,organ : organString as String)
        print("find Organ donor")
    }
    
    //MARK: API Response call
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
        for (key, value) in response {
            appConstants.hideLoadingHUD(for_view: self.view)
            print("key=\(key), value=\(value)")
            if let responseDictionary : NSDictionary = value as? NSDictionary
            {
               if let donorList : NSArray = responseDictionary .value(forKey: "donorlist") as? NSArray
               {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let FindDonorListVC = storyboard.instantiateViewController(withIdentifier: "FindDonorListVC") as! FindDonorListVC
                FindDonorListVC.donorListArray = donorList
                FindDonorListVC.headerString = "Organ Donor List"
                self.navigationController?.pushViewController(FindDonorListVC, animated: true)
            }
                else
               {
                let MessageString = "No Data Found"
                appConstants.showAlert(title: "Oops", message: MessageString, controller: self)
                }
            }
            else{
                let MessageString = "No Data Found"
                appConstants.showAlert(title: "Oops", message: MessageString, controller: self)
            }
        }
    }
    
    //MARK: Get blood Group List
    @IBAction func GetBloodGroupListAction()
    {
        tableview?.isHidden = false
    }
    //MARK: Get Organ List
    @IBAction func GetOrganListAction()
    {
        tableviewOrgan?.isHidden = false
    }
    //MARK: Hide Table View
    func dismissView() {
        tableview?.isHidden = true
    }
    
    //MARK: TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("table view tag \(tableView.tag)")
        
        if(tableView == tableview)
        {
        return bloodgroupArray.count
    }
        else{
            return organListArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview?.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)
        cell?.contentView.backgroundColor = UIColor.groupTableViewBackground
        cell?.textLabel?.backgroundColor = UIColor.groupTableViewBackground
        
         if(tableView == tableview)
        {
        cell?.textLabel!.text = "\(bloodgroupArray[indexPath.row])"
        }
        else{
           cell?.textLabel!.text = "\(organListArray[indexPath.row])"
        }
        cell?.textLabel!.font = UIFont(name: (cell?.textLabel?.font.fontName)!, size:14)
        cell?.textLabel?.textAlignment = NSTextAlignment.center
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        if(tableView == tableview)
        {
        bloodGroupTextField?.text = bloodgroupArray[indexPath.row] as? String
        tableview?.isHidden = true
            tableview?.isHidden = true
        }
        else{
            organTextField?.text = organListArray[indexPath.row] as? String
            tableviewOrgan?.isHidden = true
            tableview?.isHidden = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
