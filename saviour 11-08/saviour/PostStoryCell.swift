//
//  PostStoryCell.swift
//  saviour
//
//  Created by Pooja on 20/07/17.
//  Copyright © 2017 Truway. All rights reserved.
//

import UIKit

class PostStoryCell: UITableViewCell {
    @IBOutlet weak var imgView : UIImageView?
    @IBOutlet weak var postNumberLabel : UILabel?
    @IBOutlet weak var nameLabel : UILabel?
    @IBOutlet weak var timeLabel : UILabel?
    @IBOutlet weak var txtView : UITextView?
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
