//
//  PostStoryVC.swift
//  saviour
//
//  Created by Pooja on 20/07/17.
//  Copyright © 2017 Truway. All rights reserved.
//

import UIKit

class PostStoryVC: UIViewController, UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var tableView : UITableView?
    @IBOutlet weak var newPostButton : UIButton?
    
    var postStoryArr : NSMutableArray = ["ghsgdahgcvhjgdsggsvhsgvhgvhjgdshcghgcdhgcghsghchghcghjdgghdhhhgsghc","sdvgcndsvchvsdhchgdhgjchcghgnbbbbbbbbbghsgdahgcvhjgdsggsvhsgvhgvhjgdshcghgcdhgcghsghchghcghjdgghdhhhgsghc","sdvgcndsvchvsdhchgdhgjchcghgnbbbbbbbbbghsgdahgcvhjgdsggsvhsgvhgvhjgdshcghgcdhgcghsghchghcghjdgghdhhhgsghc","sdvgcndsvchvsdhchgdhgjchcghgnbbbbbbbbbghsgdahgcvhjgdsggsvhsgvhgvhjgdshcghgcdhgcghsghchghcghjdgghdhhhgsghc","frgqsfxgh42561456416w426sdvgcndsvchvsdhchgdhgjchcghgnbbbbbbbbbghsgdahgcvhjgdsggsvhsgvhgvhjgdshcghgcdhgcghsghchghcghjdgghdhhhgsghc76517457623465425646565246532654265324652365625465246256462354653275472346"]
    
    override func viewWillAppear(_ animated: Bool) {
        newPostButton?.layer.cornerRadius = 10.0
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView?.estimatedRowHeight = 100.0
        tableView?.rowHeight = UITableViewAutomaticDimension
        // Do any additional setup after loading the view.
    }
    
    //MARK: TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell") as! PostStoryCell!
        cell?.txtView?.text = postStoryArr[indexPath.row] as! String
       
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
       
    }
    
    //MARK: Write new post button click
    @IBAction func writePost(sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let WritePostVC = storyboard.instantiateViewController(withIdentifier: "WritePostVC") as! WritePostVC
        self.navigationController?.pushViewController(WritePostVC, animated: false)
    }
    
    //MARK: Back button clicked
    @IBAction func BackClicked(sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ViewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(ViewController, animated: false)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
