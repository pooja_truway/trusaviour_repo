//
//  SearchListCell.swift
//  saviour
//
//  Created by Truway India on 26/07/17.
//  Copyright © 2017 Truway. All rights reserved.
//

import UIKit

class SearchListCell: UITableViewCell {
    
    @IBOutlet weak var imgView : UIImageView?
    @IBOutlet weak var nameLabel : UILabel?
    @IBOutlet weak var mobileLabel : UILabel?
    @IBOutlet weak var bloodGroupLabel : UILabel?
    @IBOutlet weak var cityLabel : UILabel?
   


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
