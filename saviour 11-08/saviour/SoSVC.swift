//
//  SoSVC.swift
//  saviour
//
//  Created by Pooja on 21/07/17.
//  Copyright © 2017 Truway. All rights reserved.
//

import UIKit
import CoreLocation
import MessageUI

class SoSVC: UIViewController,CLLocationManagerDelegate,MFMessageComposeViewControllerDelegate {
    
    @IBOutlet weak var sendMsgButton : UIButton?
    let locationManager = CLLocationManager()
    var street : NSString = ""
    var locality : NSString = ""
    var state : NSString = ""
    var country : NSString = ""
    
    var contact1 : String = ""
    var contact2 : String = ""
    var contact3 : String = ""
    
    var messageContent : String = ""
    var locationObj : CLLocation?
    var contactsArray : NSMutableArray = []
    
    var locationArray : NSArray = []
    var appConstants : AppConstants = AppConstants()
    var apiManager : ApiManager = ApiManager()

    override func viewDidLoad() {
     super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        sendMsgButton?.backgroundColor = UIColor().HexToColor(hexString: appConstants.nameInfoView_brighterColor).withAlphaComponent(1.0)
        
//        locationManager.startUpdatingLocation()
    // Do any additional setup after loading the view.
    }
    
    
    private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.first {
            
            locationArray = locations as NSArray
            let MylocationObj = locationArray.lastObject as! CLLocation
            locationObj = MylocationObj
            print("last object array \(MylocationObj)")
            self.locationManager.stopUpdatingLocation()
           if let contactNumber1 : String = self.appConstants.defaults.value(forKey: "contact1Mobile") as? String
           {
             contact1 = contactNumber1
            contactsArray.insert(contact1, at: 0)
            }
            if let contactNumber2 : String = self.appConstants.defaults.value(forKey: "contact2Mobile") as? String
            {
               contact2 = contactNumber2
                print("contact2 : \(contact2)")
                contactsArray.insert(contact2, at: 0)

            }
           if let contactNumber3 : String = self.appConstants.defaults.value(forKey: "contact3Mobile") as? String
           {
             contact3 = contactNumber3
            contactsArray.insert(contact3, at: 0)

            }
            if MFMessageComposeViewController.canSendText() == true {
                let recipients = "\(contact1),\(contact2),\(contact3)"
                
                
                print("recipients : \(recipients)")
               let recipientsArray = recipients.components(separatedBy: ",")
                let messageController = MFMessageComposeViewController()
                messageController.delegate = self as? UINavigationControllerDelegate
                messageController.messageComposeDelegate  = self as? MFMessageComposeViewControllerDelegate
                messageController.recipients = recipientsArray
                
                let url = "Emergency SOS..My current location is : https://maps.google.com/maps/dir//\(MylocationObj.coordinate.latitude),\(MylocationObj.coordinate.longitude)"
                print("location message \(url)")
                messageContent = "Emergency SOS..My current location is : https://maps.google.com/maps/dir//\(MylocationObj.coordinate.latitude),\(MylocationObj.coordinate.longitude)"
                
                messageController.body = url
                
                self.present(messageController, animated: true, completion: nil)
            } else {
                //handle text messaging not available
            }
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController,
                                      didFinishWith result: MessageComposeResult) {
        
        print("result :>> \(MessageComposeResult.cancelled)")
        
        switch result.rawValue
        {
        case 0 :
            print("message cancelled by user")
            
        case 1 :
            print("message successfully sent")
            SendMessageReportApiCall()
        default :
            break
        }
        controller.dismiss(animated: true, completion: nil)
    }
    
     //MARK: Send Message and location button clicked
    @IBAction func SendLocation(sender : UIButton)
    {
        locationManager.startUpdatingLocation()
    }
    
    //MARK: Location Manager didFail Method
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
    
    //MARK: Api call to report message sent to contacts
    func SendMessageReportApiCall()
    {
        let userId : Int = appConstants.defaults.value(forKey: "userId")as! Int
        let coord = locationObj?.coordinate
        let longitude = coord?.longitude //Latitude & Longitude as String
        let latitude = coord?.latitude
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy hh:mm:ss a"
        let currentTime = formatter.string(from: date)
        print("array count : \(contactsArray.count)")
        for i in 0..<3 {
            let toNumber : String = contactsArray.object(at: i) as! String
            
            print("to number \(toNumber)")
            if(toNumber == "")
            {
                
            }
            else{
            self.apiManager.SendMessageReportApiCall(action: "smsDetails",userId:userId,message:messageContent,toNumber: toNumber,latitude: latitude!,longitude:longitude!,time: currentTime)
            }
        }
        contactsArray.removeAllObjects()
       
        print("message reported to server")
    }

    
    //MARK: Back button clicked
    @IBAction func BackClicked(sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ViewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(ViewController, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
