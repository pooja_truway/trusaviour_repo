//
//  ViewController.swift
//  saviour
//
//  Created by Pooja on 27/06/17.
//  Copyright © 2017 Truway. All rights reserved.
//

import UIKit
import ContactsUI


class ViewController: UIViewController , UIScrollViewDelegate, UITextFieldDelegate,apiManagerDelegate,UITableViewDelegate,UITableViewDataSource,CNContactPickerDelegate{
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var nameInfoView: UIView!
    
    @IBOutlet weak var tableview : UITableView?
    
    @IBOutlet weak var contact1View: UIView!
    @IBOutlet weak var contact2View: UIView!
    @IBOutlet weak var contact3View: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var edit1_Button: UIButton!
    @IBOutlet weak var contact1Button: UIButton!
    @IBOutlet weak var contact2Button: UIButton!
    @IBOutlet weak var contact3Button: UIButton!
    
    @IBOutlet weak var saveProfileButton: UIButton!
    
    @IBOutlet weak var username_TextField : UITextField?
    @IBOutlet weak var usermobile_TextField : UITextField?
    @IBOutlet weak var userbloodgroup_TextField : UITextField?
    
    @IBOutlet weak var cont1name_TextField : UITextField?
    @IBOutlet weak var cont1mobile_TextField : UITextField?
    @IBOutlet weak var cont1relation_TextField : UITextField?
    
    @IBOutlet weak var cont2name_TextField : UITextField?
    @IBOutlet weak var cont2mobile_TextField : UITextField?
    @IBOutlet weak var cont2relation_TextField : UITextField?
    
    @IBOutlet weak var cont3name_TextField : UITextField?
    @IBOutlet weak var cont3mobile_TextField : UITextField?
    @IBOutlet weak var cont3relation_TextField : UITextField?
    
    @IBOutlet weak var NameLabel : UILabel?
    var userName_string : NSString = ""
    var userMobile_string : NSString = ""
    var userBloodGroup_string : NSString = ""
    
     var contact1Name_string : NSString = ""
     var contact1Mobile_string : NSString = ""
     var contact1Relation_string : NSString = ""
    
     var contact2Name_string : NSString = ""
     var contact2Mobile_string : NSString = ""
     var contact2Relation_string : NSString = ""
    
     var contact3Name_string : NSString = ""
     var contact3Mobile_string : NSString = ""
     var contact3Relation_string : NSString = ""
    
    var MessageString : String = ""
    var bloodgroupArray : NSArray = ["Not Sure","A+","A-","B+","B-","AB+","AB-","O+","O-"]
    var numberofContactsArray : NSMutableArray = []
    @IBOutlet weak var bloodgrouptableShowButton : UIButton?
    
    var appConstants : AppConstants = AppConstants()
    var apiManager : ApiManager = ApiManager()
    
    //MARK: ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {
    tableview?.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        
        if let userId : Int = appConstants.defaults.value(forKey: "userId") as? Int{
        if userId == 0
        {
        okButton?.isHidden = true
        menuButton?.isHidden = true
        saveProfileButton?.isHidden = false
            username_TextField?.isUserInteractionEnabled = true
            usermobile_TextField?.isUserInteractionEnabled = true
            userbloodgroup_TextField?.isUserInteractionEnabled = true
        }
        else{
            saveProfileButton?.isHidden = true
            okButton?.isHidden = false
            menuButton?.isHidden = false
            username_TextField?.text =  appConstants.defaults.value(forKey: "userName") as? String
            usermobile_TextField?.text = appConstants.defaults.value(forKey: "userMobile") as? String
            userbloodgroup_TextField?.text = appConstants.defaults.value(forKey: "userBloodGroup") as? String
            username_TextField?.isUserInteractionEnabled = false
            usermobile_TextField?.isUserInteractionEnabled = false
            userbloodgroup_TextField?.isUserInteractionEnabled = false
            bloodgrouptableShowButton?.isUserInteractionEnabled = false
        }
        }
        else{
            okButton?.isHidden = true
            menuButton?.isHidden = true
            saveProfileButton?.isHidden = false
            username_TextField?.isUserInteractionEnabled = true
            usermobile_TextField?.isUserInteractionEnabled = true
            userbloodgroup_TextField?.isUserInteractionEnabled = true
        }
        
        if(username_TextField?.text == "")
        {
         nameInfoView.backgroundColor = UIColor().HexToColor(hexString: appConstants.nameInfoView_color).withAlphaComponent(0.7)
        }
        else{
           nameInfoView.backgroundColor = UIColor().HexToColor(hexString: appConstants.nameInfoView_brighterColor).withAlphaComponent(0.7)
        }
        
        tableview?.isHidden = true
        
        okButton.backgroundColor = UIColor().HexToColor(hexString: appConstants.nameInfoView_color).withAlphaComponent(0.7)
        okButton.layer.cornerRadius = 7.0
        
        saveProfileButton.backgroundColor = UIColor().HexToColor(hexString: appConstants.nameInfoView_color).withAlphaComponent(0.7)
        saveProfileButton.layer.cornerRadius = 7.0

        
        bottomView.backgroundColor = UIColor().HexToColor(hexString: appConstants.nameInfoView_color).withAlphaComponent(0.7)
        
         scrollview?.contentSize = CGSize(width: self.view.frame.size.width, height: 600)
        
        contact1View.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        contact1View.layer.cornerRadius = 2.0
        contact2View.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        contact2View.layer.cornerRadius = 2.0
        contact3View.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        contact3View.layer.cornerRadius = 2.0
        
        //MARK: Make all fields user interaction false initially
        
        
        cont1name_TextField?.isUserInteractionEnabled = true
        cont1mobile_TextField?.isUserInteractionEnabled = true
        cont1relation_TextField?.isUserInteractionEnabled = true
        
        cont2name_TextField?.isUserInteractionEnabled = true
        cont2mobile_TextField?.isUserInteractionEnabled = true
        cont2relation_TextField?.isUserInteractionEnabled = true
        
        cont3name_TextField?.isUserInteractionEnabled = true
        cont3mobile_TextField?.isUserInteractionEnabled = true
        cont3relation_TextField?.isUserInteractionEnabled = true
        
        userbloodgroup_TextField?.autocapitalizationType = UITextAutocapitalizationType.allCharacters // to show caps character in blood group field
        
          SetUI() //TO set values in UI
    }
    
    @IBAction func GetBloodGroupListAction()
    {
        tableview?.isHidden = false
        contact1View.isUserInteractionEnabled = false
        contact2View.isUserInteractionEnabled = false
        contact3View.isUserInteractionEnabled = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.apiManager = ApiManager()
        apiManager.delegate = self
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    @IBAction func click_Contact(_ sender: UIButton) {
        let cnPicker = CNContactPickerViewController()
        cnPicker.delegate = self
        
        print(sender.tag)
        appConstants.defaults.set(sender.tag as Int, forKey: "tag")
        self.present(cnPicker, animated: true, completion: nil)
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "addNewContact"), object: nil, userInfo: ["contactToAdd": contact])
        
       if let tag  = appConstants.defaults.value(forKey: "tag")
        
       {
        let tag1 : Int = tag as! Int
        print("tag : \(tag)")
        if(tag1 == 1)
        {
          self.cont1name_TextField?.text = contact.givenName + " " + contact.familyName
            if contact.phoneNumbers.count > 0 {
                self.cont1mobile_TextField?.text = (contact.phoneNumbers[0].value ).value(forKey: "digits") as? String
                
                contact1Name_string = (cont1name_TextField?.text)! as NSString
                contact1Mobile_string = (cont1mobile_TextField?.text)! as NSString
                
                appConstants.defaults.set(contact1Name_string, forKey: "contact1Name")
                appConstants.defaults.set(contact1Mobile_string, forKey: "contact1Mobile")
            }
        }
        else if (tag1 == 2)
        {
            self.cont2name_TextField?.text = contact.givenName + " " + contact.familyName
            if contact.phoneNumbers.count > 0 {
                self.cont2mobile_TextField?.text = (contact.phoneNumbers[0].value ).value(forKey: "digits") as? String
                
                contact2Name_string = (cont2name_TextField?.text)! as NSString
                contact2Mobile_string = (cont2mobile_TextField?.text)! as NSString
                
                appConstants.defaults.set(contact2Name_string, forKey: "contact2Name")
                appConstants.defaults.set(contact2Mobile_string, forKey: "contact2Mobile")
                
            }
        }
        else{
            self.cont3name_TextField?.text = contact.givenName + " " + contact.familyName
            if contact.phoneNumbers.count > 0 {
                self.cont3mobile_TextField?.text = (contact.phoneNumbers[0].value ).value(forKey: "digits") as? String
                
                contact3Name_string = (cont3name_TextField?.text)! as NSString
                contact3Mobile_string = (cont3mobile_TextField?.text)! as NSString
                
                appConstants.defaults.set(contact3Name_string, forKey: "contact3Name")
                appConstants.defaults.set(contact3Mobile_string, forKey: "contact3Mobile")
            }
        }
        }
        
    }

    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        
        picker.dismiss(animated: true, completion: nil)
        
    }

    
//    //MARK: To limit characters in text field
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
//        switch (textField.tag){
//            
//        case 1: // for user mobile
//            let maxLength = 10
//            let currentString: NSString = usermobile_TextField!.text! as NSString
//            let newString: NSString =
//                currentString.replacingCharacters(in: range, with: string) as NSString
//            return newString.length <= maxLength
//            
//        case 4: // for contact1 mobile
//            let maxLength = 12
//            let currentString: NSString = cont1mobile_TextField!.text! as NSString
//            let newString: NSString =
//                currentString.replacingCharacters(in: range, with: string) as NSString
//            return newString.length <= maxLength
//            
//        case 7: // for contact2 mobile
//            let maxLength = 12
//            let currentString: NSString = cont2mobile_TextField!.text! as NSString
//            let newString: NSString =
//                currentString.replacingCharacters(in: range, with: string) as NSString
//            return newString.length <= maxLength
//            
//        case 10: // for contact3 mobile
//            let maxLength = 12
//            let currentString: NSString = cont3mobile_TextField!.text! as NSString
//            let newString: NSString =
//                currentString.replacingCharacters(in: range, with: string) as NSString
//            return newString.length <= maxLength
//        default:
//            break
//        }
//        return true
//    }
    
 //MARK: TO set values in UI
    func SetUI()
    {
        username_TextField?.text =  appConstants.defaults.value(forKey: "userName") as? String
        usermobile_TextField?.text = appConstants.defaults.value(forKey: "userMobile") as? String
        userbloodgroup_TextField?.text = appConstants.defaults.value(forKey: "userBloodGroup") as? String
        
        cont1name_TextField?.text = appConstants.defaults.value(forKey: "contact1Name") as? String
        cont1mobile_TextField?.text = appConstants.defaults.value(forKey: "contact1Mobile") as? String
        cont1relation_TextField?.text = appConstants.defaults.value(forKey: "contact1Relation") as? String
        
        cont2name_TextField?.text = appConstants.defaults.value(forKey: "contact2Name") as? String
        cont2mobile_TextField?.text = appConstants.defaults.value(forKey: "contact2Mobile") as? String
        cont2relation_TextField?.text = appConstants.defaults.value(forKey: "contact2Relation") as? String
        
        cont3name_TextField?.text = appConstants.defaults.value(forKey: "contact3Name") as? String
        cont3mobile_TextField?.text = appConstants.defaults.value(forKey: "contact3Mobile") as? String
        cont3relation_TextField?.text = appConstants.defaults.value(forKey: "contact3Relation") as? String
        
        NameLabel?.text =  appConstants.defaults.value(forKey: "infoLabel") as? String
    }
    
    //MARK: Next Button Click
    @IBAction func NextClicked(sender : UIButton)
    {
        SaveToDefaults()
        
        print("array count : \(numberofContactsArray.count)")
         print("array  : \(numberofContactsArray)")
        appConstants.defaults.set(numberofContactsArray.count, forKey: "numberofContactsAdded")
        
        userMobile_string = (usermobile_TextField?.text)! as NSString
        contact1Name_string = (cont1name_TextField?.text)! as NSString
        contact1Mobile_string = (cont1mobile_TextField?.text)! as NSString
        
        contact2Name_string = (cont2name_TextField?.text)! as NSString
        contact2Mobile_string = (cont2mobile_TextField?.text)! as NSString
        
        contact3Name_string = (cont3name_TextField?.text)! as NSString
        contact3Mobile_string = (cont3mobile_TextField?.text)! as NSString
        
        if(username_TextField?.text?.trimmingCharacters(in: .whitespaces).isEmpty)! || (usermobile_TextField?.text?.trimmingCharacters(in: .whitespaces).isEmpty)! || (userbloodgroup_TextField?.text?.trimmingCharacters(in: .whitespaces).isEmpty)!
        {
            MessageString = "Please Provide User Name/Mobile/BloodGroup To Help Someone Save You"
            appConstants.showAlert(title: "Oops", message: MessageString, controller: self)
        }
       

    else if  (contact1Mobile_string == "") && (contact1Name_string != "")
        {
            MessageString = "Please Add Mobile Number for Contact 1"
            appConstants.showAlert(title: "Oops", message: MessageString, controller: self)
        }
        
        else if  (contact1Mobile_string != "") && (contact1Name_string == "")
        {
            MessageString = "Please Add Name for Contact 1"
            appConstants.showAlert(title: "Oops", message: MessageString, controller: self)
        }
            
        else if  (contact2Mobile_string == "") && (contact2Name_string != "")
        {
            MessageString = "Please Add Mobile Number for Contact 2"
            appConstants.showAlert(title: "Oops", message: MessageString, controller: self)
        }
        else if  (contact2Mobile_string != "") && (contact2Name_string == "")
        {
            MessageString = "Please Add Name for Contact 2"
            appConstants.showAlert(title: "Oops", message: MessageString, controller: self)
        }
        
        else if  (contact3Mobile_string == "") && (contact3Name_string != "")
        {
            MessageString = "Please Add Mobile Number for Contact 3"
            appConstants.showAlert(title: "Oops", message: MessageString, controller: self)
        }
        else if  (contact3Mobile_string != "") && (contact3Name_string == "")
        {
            MessageString = "Please Add Name for Contact 3"
            appConstants.showAlert(title: "Oops", message: MessageString, controller: self)
        }
                    
        else if  (contact1Mobile_string == "") && (contact1Name_string == "") && (contact2Mobile_string == "") && (contact2Name_string == "") && (contact3Mobile_string == "") && (contact3Name_string == "")
        {
            MessageString = "Please Enter Atleast 1 Emergency Contact Detail"
            appConstants.showAlert(title: "Oops", message: MessageString, controller: self)
        }
            
    else{
            print("proceed to next page")
                //call api to send all info to server
                sendInfo()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ChooseTempleteVC = storyboard.instantiateViewController(withIdentifier: "ChooseTempleteVC1") as! ChooseTempleteVC
            ChooseTempleteVC.contactsArray = numberofContactsArray
            self.navigationController?.pushViewController(ChooseTempleteVC, animated: true)
        }
    }
    //MARK: TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bloodgroupArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview?.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)
         cell?.contentView.backgroundColor = UIColor.groupTableViewBackground
        cell?.textLabel?.backgroundColor = UIColor.groupTableViewBackground
        cell?.textLabel!.text = "\(bloodgroupArray[indexPath.row])"
        cell?.textLabel!.font = UIFont(name: (cell?.textLabel?.font.fontName)!, size:14)
        cell?.textLabel?.textAlignment = NSTextAlignment.center
       
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        userbloodgroup_TextField?.text = bloodgroupArray[indexPath.row] as? String
        tableview?.isHidden = true
        contact1View.isUserInteractionEnabled = true
        contact2View.isUserInteractionEnabled = true
        contact3View.isUserInteractionEnabled = true
    }
//MARK: Send info to server
    func sendInfo()
    {
        
        let device_token = appConstants.defaults.value(forKey: "notification_token")
        self.apiManager.ApiSendInfoToServer(action: "savierUserDetails",userName:
            
            userMobile_string as String,userMobile: userMobile_string as String,userBloodGroup : userBloodGroup_string as String,contact1Name:contact1Name_string as String,contact1Mobile:contact1Mobile_string as String,contact1Relation: contact1Relation_string as String,contact2Name:contact2Name_string as String,contact2Mobile:contact2Mobile_string as String,contact2Relation:contact2Relation_string as String,contact3Name:contact3Name_string as String,contact3Mobile:contact3Mobile_string as String,contact3Relation:contact3Relation_string as String,device_token:device_token as! String)
    }
    
    //MARK: Save Profile Info Button Clicked
    @IBAction func saveProfileAction(sender : UIButton)
    {
        if(username_TextField?.text?.trimmingCharacters(in: .whitespaces).isEmpty)! || (usermobile_TextField?.text?.trimmingCharacters(in: .whitespaces).isEmpty)! || (userbloodgroup_TextField?.text?.trimmingCharacters(in: .whitespaces).isEmpty)!
        {
            MessageString = "Please Provide User Name/Mobile/BloodGroup To Help Someone Save You"
            appConstants.showAlert(title: "Oops", message: MessageString, controller: self)
        }
        else if  (userMobile_string != "")
        {
            MessageString = "Please Enter Valid Mobile Number"
            appConstants.showAlert(title: "Oops", message: MessageString, controller: self)
        }
        
        else{
            userName_string = (username_TextField?.text)! as NSString
            userMobile_string = (usermobile_TextField?.text)! as NSString
            userBloodGroup_string = (userbloodgroup_TextField?.text)! as NSString
            saveProfileInfo()
        }
    }
    
    //MARK: Save Profile info to server
    func saveProfileInfo()
    {
       let device_token = appConstants.defaults.value(forKey: "notification_token")
       let deviceId = appConstants.defaults.value(forKey: "deviceID")
       // let device_token = "fhsafdcghfsdhgcfsdfcgh " //for simulator
       // let deviceId = "fsadfhcgfask"
       appConstants.showLoadingHUD(to_view: self.view)
        self.apiManager.ApiSaveProfileInfoToServer(action: "saveProfileDetails",userName:
            userName_string as String,userMobile: userMobile_string as String,userBloodGroup : userBloodGroup_string as String,device_token:device_token as! String,deviceId : deviceId as! String,userId : "")
        
        print(userName_string)
        print(userMobile_string)
        print(userBloodGroup_string)
       // print(deviceId!)
       // print(device_token!)
        
    }

    
    
    
    //MARK: SAve values to defaults
    func SaveToDefaults()
    {
        numberofContactsArray.removeAllObjects()
        userName_string = (username_TextField?.text)! as NSString
        userMobile_string = (usermobile_TextField?.text)! as NSString
        userBloodGroup_string = (userbloodgroup_TextField?.text)! as NSString
        
        appConstants.defaults.set(userName_string, forKey: "userName")
        appConstants.defaults.set(userMobile_string, forKey: "userMobile")
        appConstants.defaults.set(userBloodGroup_string, forKey: "userBloodGroup")
        
        contact1Name_string = (cont1name_TextField?.text)! as NSString
        contact1Mobile_string = (cont1mobile_TextField?.text)! as NSString
        contact1Relation_string = (cont1relation_TextField?.text)! as NSString
        if(contact1Mobile_string != "")
        {
            numberofContactsArray.insert(contact1Mobile_string, at: 0)
        }
        
       //contact2
        contact2Name_string = (cont2name_TextField?.text)! as NSString
        contact2Mobile_string = (cont2mobile_TextField?.text)! as NSString
        contact2Relation_string = (cont2relation_TextField?.text)! as NSString
        if(contact2Mobile_string != "")
        {
            numberofContactsArray.insert(contact2Mobile_string, at: 0)
        }
      
        //contact3
        contact3Name_string = (cont3name_TextField?.text)! as NSString
        contact3Mobile_string = (cont3mobile_TextField?.text)! as NSString
        contact3Relation_string = (cont3relation_TextField?.text)! as NSString
        if(contact3Mobile_string != "")
        {
            numberofContactsArray.insert(contact3Mobile_string, at: 0)
        }
       
        //Contact1 details
        appConstants.defaults.set(contact1Name_string, forKey: "contact1Name")
        appConstants.defaults.set(contact1Mobile_string, forKey: "contact1Mobile")
        appConstants.defaults.set(contact1Relation_string, forKey: "contact1Relation")
        
        appConstants.defaults.set(contact2Name_string, forKey: "contact2Name")
        appConstants.defaults.set(contact2Mobile_string, forKey: "contact2Mobile")
        appConstants.defaults.set(contact2Relation_string, forKey: "contact2Relation")
        
        appConstants.defaults.set(contact3Name_string, forKey: "contact3Name")
        appConstants.defaults.set(contact3Mobile_string, forKey: "contact3Mobile")
        appConstants.defaults.set(contact3Relation_string, forKey: "contact3Relation")
    }
    
    
    //MARK: Menu Button Clicked
    @IBAction func menuButtonClicked(sender : UIButton)
    {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let MenuItemsVC = storyboard.instantiateViewController(withIdentifier: "MenuItemsVC") as! MenuItemsVC
            MenuItemsVC.view.frame = CGRect(x: 0,y: 0, width: self.view.frame.width , height: self.view.frame.height )
            self.addChildViewController(MenuItemsVC)
            self.view.addSubview(MenuItemsVC.view)
            MenuItemsVC.didMove(toParentViewController: self)
    }
    
    //MARK: Notification button clicked
    @IBAction func notificationButtonClicked(sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let NotificationVC = storyboard.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
       self.navigationController?.pushViewController(NotificationVC, animated: true)
    }
    
    //MARK: Share App Button
    @IBAction func shareButtonClicked(sender : UIButton)
        {
        let textToShare = "Hey check out our app at : "
        if let myWebsite = NSURL(string: "https://itunes.apple.com/us/app/tru-saviour/id1255052252?is=1&mt=8") {
        let objectsToShare = [textToShare, myWebsite] as [Any]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = sender
        self.present(activityVC, animated: true, completion: nil)
        }
}
    
    //MARK: API Response call
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
        appConstants.hideLoadingHUD(for_view: self.view)
        for (key, value) in response {
            print("key=\(key), value=\(value)")
            
            let responseDictionary : NSDictionary = value as! NSDictionary
            let responseMessage : NSString = responseDictionary .value(forKey: "message") as! NSString
            if(responseMessage.isEqual(to: "success"))
            {
            let userId : Int = responseDictionary .value(forKey: "userId") as! Int
            appConstants.defaults.set(userId, forKey: "userId")
            appConstants.defaults.set(userName_string, forKey: "userName")
            appConstants.defaults.set(userMobile_string, forKey: "userMobile")
            appConstants.defaults.set(userBloodGroup_string, forKey: "userBloodGroup")
                saveProfileButton?.isHidden = true
                okButton?.isHidden = false
                menuButton?.isHidden = false
                
    username_TextField?.isUserInteractionEnabled = false
    usermobile_TextField?.isUserInteractionEnabled = false
    userbloodgroup_TextField?.isUserInteractionEnabled = false
    bloodgrouptableShowButton?.isUserInteractionEnabled = false
            }
            else{
                
            }
        }
    }

 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
