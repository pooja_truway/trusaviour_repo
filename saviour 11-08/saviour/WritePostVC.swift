//
//  WritePostVC.swift
//  saviour
//
//  Created by Pooja on 20/07/17.
//  Copyright © 2017 Truway. All rights reserved.
//

import UIKit

class WritePostVC: UIViewController,UITextViewDelegate {
    @IBOutlet weak var postHeading : UITextField?
    @IBOutlet weak var postTextView : UITextView?
    @IBOutlet weak var postButton : UIButton?
    
    
    override func viewWillAppear(_ animated: Bool) {
        postButton?.layer.cornerRadius = 10
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        postTextView?.text = "Write your post here..."
        postTextView?.textColor = UIColor.darkGray
        postTextView?.delegate = self

        // Do any additional setup after loading the view.
    }
    
    //MARK: Text view methods
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        postTextView?.text = ""
        postTextView?.textColor = UIColor.darkGray
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if (postTextView?.text.characters.count ?? 0) == 0 {
            postTextView?.textColor = UIColor.darkGray
            postTextView?.text = "Write your post here..."
            postTextView?.resignFirstResponder()
        }
    }
    
    //MARK: Back button clicked
    @IBAction func BackClicked(sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Post button clicked
    @IBAction func PostClicked(sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
